var $ = jQuery;

function reloadFacet() {
    $('.facetwp-facet').each(function() {
        if ($(this).children('.facet-inner').length == 0) {
            var moreCount = $(this).children('.facetwp-overflow').children('.facetwp-checkbox').length;
            $(this).children('div').wrapAll('<div class="facet-inner" />');
            $(this).children('.facetwp-toggle:eq(0)').text("See " + moreCount + " more");
        }
    });
}
$(document).on('facetwp-refresh', function() { reloadFacet(); });
$(document).on('facetwp-loaded', function() {
    $.each(FWP.settings.num_choices, function(key, val) { var html = $('.facetwp-facet-' + key).html(); if (html == "") { $('.facetwp-facet-' + key).parent().hide() } else { $('.facetwp-facet-' + key).parent().show(); } });
    reloadFacet();
});
reloadFacet();

function fr_slider_init(ob) {
    $(ob).each(function() {
        var default_settings = { slidesToScroll: 1, slidesToShow: 1 }
        var settings = fr_parse_attr_data($(this).attr("data-fr"));
        settings = $.extend(default_settings, settings);
        settings = fr_apply_filter("fr_slider_init", settings, [$(ob)]);
        $(ob).find(".slides").slick(settings);
        $(ob).find(".slides").on('beforeChange', function(event, slick, currentSlide, nextSlide) { next_slide(this, nextSlide); });

        function next_slide(ob, nextSlide) {
            if (nextSlide == 0) { $(ob).parents(".fr-slider").first().find(".arrow.prev").animate({ opacity: 0 }); } else { $(ob).parents(".fr-slider").first().find(".arrow.prev").animate({ opacity: 1 }); }
            if (nextSlide >= $(ob).find(".slide").length - settings.slidesToScroll) { $(ob).parents(".fr-slider").first().find(".arrow.next").animate({ opacity: 0 }); } else { $(ob).parents(".fr-slider").first().find(".arrow.next").animate({ opacity: 1 }); }
        }
        next_slide($(ob).find(".slides"), 0);
        $(ob).addClass("init");
    });
}
$(window).ready(function() {
    $('.fl-callout .fl-callout-title').each(function() {
        $(this).next('.fl-callout-text-wrap').andSelf().wrapAll('<div class="fl-callout-content-wrap"/>');
    });
    if ($('.toggle-image-thumbnails > div').length > 7) {
        if ($('.toggle-image-thumbnails').hasClass('vertical-slider')) {
            $('.toggle-image-thumbnails').slick({
                vertical: true,
                slidesToScroll: 6,
                slidesToShow: 6,
                arrows: true,
                infinite: false,
                prevArrow: '<a href="javascript:void(0)" class="arrow prev"><i class="fa fa-angle-up"></i></a>',
                nextArrow: '<a href="javascript:void(0)" class="arrow next"><i class="fa fa-angle-down"></i></a>'
            });
        } else {
            $('.toggle-image-thumbnails').slick({
                slidesToScroll: 7,
                slidesToShow: 7,
                arrows: true,
                infinite: false,
                prevArrow: '<a href="javascript:void(0)" class="arrow prev"><i class="fa fa-angle-left"></i></a>',
                nextArrow: '<a href="javascript:void(0)" class="arrow next"><i class="fa fa-angle-right"></i></a>'
            });
        }
    }
    $('.toggle-image-thumbnails .toggle-image-holder a').click(function(e) {
        e.preventDefault();
        $('.toggle-image-thumbnails .toggle-image-holder a').removeClass('active');
        url = $(this).attr("data-background");
        bkImg = "url('" + url + "')";
        $($(this).attr("data-fr-replace-bg")).css("background-image", bkImg);
        $(this).addClass('active');
    });
    fr_add_filter("fr_slider_init", function(settings, slider) {
        if ($(window).width() < 726) {
            if (slider.is(".color_variations_slider")) {
                settings.slidesToScroll = 3;
                settings.slidesToShow = 3;
            }
        }
        return settings;
    });
    if ($().slick) {
        fr_slider_init(".fr-slider");
        $("body").on("click", ".fr-slider .prev", function(e) {
            e.preventDefault();
            $(this).parents(".fr-slider").first().find(".slides").slick('slickPrev');
        });
        $("body").on("click", ".fr-slider .next", function(e) {
            e.preventDefault();
            $(this).parents(".fr-slider").first().find(".slides").slick('slickNext');
        });
    }

    function fr_slider_delete(ob) { $(ob).each(function() { $(this).find(".slides").slick('unslick'); }); }
    $("body").on("click", "[data-fr-replace-src]", function(e) {
        e.preventDefault();
        url = $(this).attr("data-src");
        bkImg = "url('" + url + "')";
        $($(this).attr("data-fr-replace-src")).attr("src", bkImg);
    });
    $("body").on("click", "[data-fr-replace-bg]", function(e) {
        e.preventDefault();
        url = $(this).attr("data-background");
        bkImg = "url('" + url + "')";
        $($(this).attr("data-fr-replace-bg")).css("background-image", bkImg);
    });
    $(".fr_toggle_box .box_content").each(function() {
        $(this).show();
        var dir = $(this).parents(".fr_toggle_box").first().data("dir");
        var size = $(this).outerHeight();
        if (dir == "left" || dir == "right") { size = $(this).outerWidth(); }
        $(this).css(dir, -size);
    });
    $(".fr_toggle_box .handle").click(function() {
        var parent = $(this).parents(".fr_toggle_box").first();
        var content = parent.find(".box_content").first();
        var dir = parent.data("dir");
        if (!dir) { dir = "bottom"; }
        data = {};
        if (parent.is(".active")) {
            var size = content.outerHeight();
            if (dir == "left" || dir == "right") { size = content.outerWidth(); }
            data[dir] = -size;
            content.stop().animate(data);
            parent.removeClass("active");
            parent.find(".bg").fadeOut();
        } else {
            data[dir] = 0;
            content.stop().animate(data);
            parent.addClass("active");
            parent.find(".bg").fadeIn();
        }
    });
    $(window).load(function() {
        setTimeout(function() {
            if (!$.cookie('fr_toggle_box_opened')) {
                $(".fr_toggle_box .handle").click();
                setTimeout(function() { if ($(".fr_toggle_box").is(".active")) { $(".fr_toggle_box .handle").click(); } }, 7000);
                $.cookie('fr_toggle_box_opened', 1, { expires: 365, path: '/' });
            }
        }, 1000);
    });
    var fr_link_dont_redirect = 0;
    $("body").on("click", "[data-fr-link] a", function(e) { fr_link_dont_redirect++; });
    $("body").on("click", "[data-fr-link]", function(e) { if (fr_link_dont_redirect <= 0) { window.location = $(this).attr("data-fr-link"); return false; } else { fr_link_dont_redirect--; } });
    $(".slider-menu").each(function() { $(this).height($(this).parent().height()); });
    fr_click_outside(".slider-menu", ".slider-menu", function(ob) { return $(ob).is(".active"); }, function(ob) { if ($(ob).is(".active")) { close_slider_menu(ob); } else { open_slider_menu(ob); } });
    $(".slider-menu").find(".icon").click(function() { if (!$(this).parents(".slider-menu").is(".active")) { open_slider_menu($(this).parents(".slider-menu")); } else { close_slider_menu($(this).parents(".slider-menu")); } });

    function open_slider_menu(ob) {
        $(ob).addClass("active");
        $(ob).find("ul").stop().animate({ "margin-right": 0 });
    }

    function close_slider_menu(ob) {
        $(ob).removeClass("active");
        $(ob).find("ul").stop().animate({ "margin-right": -$(ob).outerWidth() + $(ob).find(".icon").first().outerWidth() });
    }
    $("body").on("click", "a[href^='#']", function(e) { var target = $(this).attr("href"); if ($(target).length && $(target).is(".fr_popup")) { e.preventDefault(); if (!$(target).is(".active")) { fr_open_popup(target); } } });
    $("body").on("click", ".fr_popup .close_popup", function(e) { e.preventDefault(); var target = $(this).parents(".fr_popup"); if ($(target).is(".active")) { fr_close_popup(target); } });

    function fr_open_popup(target) {
        $(target).show();
        fr_center_in_window($(target).find(".content"));
        $(target).addClass("active");
    }

    function fr_close_popup(target) {
        $(target).removeClass("active");
        $(target).hide();
    }

    function fr_center_in_window(ob, offset) {
        if (!offset) { offset = 0; }
        var wt = $(window).scrollTop();
        var wh = $(window).height();
        var oh = $(ob).outerHeight();
        console.log(wh, oh, wt, offset);
        $(ob).css("top", (wh - oh) / 2 + wt + offset);
    }
    $("body").on("click", ".open-gallery-modal", function(e) {
        e.preventDefault();
        if (!$(".active-gallery-modal").length) { $(".fl-page-content").prepend("<div class='active-gallery-modal'></div>"); }
        var html = $(this).find(".gallery-modal").html();
        if ($(".active-gallery-modal").is(":visible")) { close_modal(function() { open_modal(html); }); } else { open_modal(html); }
    });
    $("body").on("click", ".active-gallery-modal .close_modal", function(e) {
        e.preventDefault();
        close_modal();
    });

    function close_modal(func) { $(".active-gallery-modal").fadeOut(func); }

    function open_modal(html) {
        var header = $(".fl-page-header").outerHeight();
        if (!$(".fl-page-header").is(":visible")) { header = $("#djcustom-header").outerHeight(); }
        console.log(header);
        $(".active-gallery-modal").css("top", header + $(window).scrollTop());
        $(".active-gallery-modal").html(html);
        $(".active-gallery-modal").fadeIn();
    }
    var hash = window.location.hash.substr(1);
    $(".fl-tabs-label").each(function() {
        if ($.trim($(this).text()).toLowerCase() == hash.toLowerCase()) {
            console.log($(this));
            change_tab($(this));
            return false;
        }
    });
    $("footer .menu-item-has-children").click(function(e) {
        if ($(window).width() <= 768) {
            e.preventDefault();
            $(this).find(">ul").toggle();
        }
    });
});

function fr_click_outside(excluded_element, element_to_hide, cond_func, hide_func) { $(document).click(function(event) { if (!$(event.target).closest(excluded_element).length) { if ((cond_func && cond_func(element_to_hide)) || (!cond_func && $(element_to_hide).is(":visible"))) { if (hide_func) { hide_func(element_to_hide); } else { $(element_to_hide).hide(); } } } }); }

function change_tab(ob) {
    ob.parent().find(".fl-tab-active").removeClass("fl-tab-active");
    ob.addClass("fl-tab-active");
    var index = ob.index();
    ob.parents(".fl-tabs").first().find(".fl-tabs-panel .fl-tabs-label").removeClass("fl-tab-active");
    ob.parents(".fl-tabs").first().find(".fl-tabs-panel .fl-tabs-panel-content").removeClass("fl-tab-active");
    ob.parents(".fl-tabs").first().find(".fl-tabs-panel:eq(" + index + ") .fl-tabs-panel-content").addClass("fl-tab-active");
}

function fr_parse_attr_data(data) {
    if (!data) { data = ""; }
    if (data.substr(0, 1) != "{") { data = "{" + data + "}"; }
    return $.parseJSON(data);
}
var fr_filters = [];

function fr_add_filter(filter, func) { filter = filter; }

function fr_apply_filter(filter, res, args) {
    if (typeof fr_filters[filter] != "undefined") {
        for (k in fr_filters[filter]) {
            var args2 = [res].concat(args);
            res = fr_filters[filter][k].apply(null, args2);
        }
    }
    return res;
}


jQuery(document).ready(function() {

    var mystore_loc = jQuery(".header_location_name").html();
    jQuery("#input_29_32").val(mystore_loc);
    console.log(mystore_loc);
    console.log('ready');
    jQuery.ajax({
       type: "POST",
       url: "/wp-admin/admin-ajax.php",
       data: 'action=get_storelisting',
       dataType: 'JSON',
       success: function(response) {
          
               var posts = JSON.parse(JSON.stringify(response));
               var header_data = posts.header;
               var list_data = posts.list;

               var mystore_loc =  jQuery.cookie("preferred_storename");
               
             
              // jQuery(".locationSelector .uabb-marketing-title").html(header_data);
               jQuery(".header_location_name").html(header_data);
             //  setTimeout(addFlyerEvent, 1000);
               jQuery("#ajaxstorelisting").html(list_data);
               //iconsClick();      
               jQuery("#input_29_32").val(mystore_loc);
       }
   });

});

jQuery(document).on('click', '.choose_location', function() {
 
   var mystore = jQuery(this).attr("data-id");
   var distance = jQuery(this).attr("data-distance");
   var storename = jQuery(this).attr("data-storename");

   jQuery.cookie("preferred_store", null, { path: '/' });
   jQuery.cookie("preferred_distance", null, { path: '/' });
   jQuery.cookie("preferred_storename", null, { path: '/' });

   jQuery.ajax({
       type: "POST",
       url: "/wp-admin/admin-ajax.php",
       data: 'action=choose_location&store_id=' + jQuery(this).attr("data-id") + '&distance=' + jQuery(this).attr("data-distance"),

       success: function(data) {

           jQuery(".header_location_name").html(data);
           jQuery.cookie("preferred_store", mystore, { expires: 1, path: '/' });
           jQuery.cookie("preferred_distance", distance, { expires: 1, path: '/' });
           jQuery.cookie("preferred_storename", storename, { expires: 1, path: '/' });

           console.log(storename);

            jQuery("#input_29_32").val(storename);

           jQuery(".uabb-offcanvas-close").trigger("click");
          
       }
   });

});

jQuery(document).ready(function() {

    var mystore_loc =  jQuery.cookie("preferred_storename");
    console.log(mystore_loc);
    jQuery(".populate-store select").val(mystore_loc);

});