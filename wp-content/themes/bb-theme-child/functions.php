<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.min.js","","",1);
    wp_enqueue_script("theme-script",get_stylesheet_directory_uri()."/script.js","","",1);
});

// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );


 
// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');


function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

if(@$_GET['keyword'] != '' && @$_GET['brand'] !="")
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
  	    setcookie('keyword' , $_GET['keyword']);
	    setcookie('brand' , $_GET['brand']);
	    wp_redirect( $url[0] );
	    exit;
 } 
 else if(@$_GET['brand'] !="" && @$_GET['keyword'] == '' )
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
        setcookie('brand' , $_GET['brand']);
        wp_redirect( $url[0] );
        exit;
 }
 else if(@$_GET['brand'] =="" && @$_GET['keyword'] != '' )
 {
        $url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $url = explode('?' , $url);
        setcookie('keyword' , $_GET['keyword']);
        wp_redirect( $url[0] );
        exit;
 }
 
 
// shortcode to show H1 google keyword fields
function new_google_keyword() 
{
      
   if( @$_COOKIE['keyword'] ==""  && @$_COOKIE['brand'] == "")
   {
        // return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on Flooring<h1>';
        return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on Flooring *<h1>';
   }
   else
   {
       $keyword = $_COOKIE['keyword'];
	   $brand = $_COOKIE['brand'];
    //    return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on '.$brand.' '.$keyword.'<h1>';
    return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on '.$brand.' '.$keyword.'<h1>';
   }
}
  add_shortcode('google_keyword_code', 'new_google_keyword');
  add_action('wp_head','cookie_gravityform_js');

  function cookie_gravityform_js()
  { // break out of php 
  ?>
  <script>
	  var brand_val ='<?php echo $_COOKIE['brand'];?>';
	  var keyword_val = '<?php echo $_COOKIE['keyword'];?>';  

      jQuery(document).ready(function($) {
      jQuery("#input_14_9").val(keyword_val);
      jQuery("#input_14_10").val(brand_val);
    });
  </script>
  <?php  
     setcookie('keyword' , '',-3600); 
     setcookie('brand' , '',-3600);
  
}

// Action to for styling H1 tag - google keyword fields
add_action('wp_head', 'add_css_head');
function add_css_head() {
    
   ?>
      <style>
          .googlekeyword {
             text-align:center;
             color: #fff;
             text-transform: capitalize;   
              /* font-size:2.5em !important; */
              font-size: 36px !important;
           }
      </style>  
   <?php 
}


$data ='';

$ddsa = unserialize(stripslashes($data));

// IP location FUnctionality
  if (!wp_next_scheduled('cde_preferred_location_cronjob')) {
    
    wp_schedule_event( time() +  17800, 'daily', 'cde_preferred_location_cronjob');
}

add_action( 'cde_preferred_location_cronjob', 'cde_preferred_location' );

function cde_preferred_location(){

          global $wpdb;

          if ( ! function_exists( 'post_exists' ) ) {
              require_once( ABSPATH . 'wp-admin/includes/post.php' );
          }

          //CALL Authentication API:
          $apiObj = new APICaller;
          $inputs = array('grant_type'=>'client_credentials','client_id'=>get_option('CLIENT_CODE'),'client_secret'=>get_option('CLIENTSECRET'));
          $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);


          if(isset($result['error'])){
              $msg =$result['error'];                
              $_SESSION['error'] = $msg;
              $_SESSION["error_desc"] =$result['error_description'];
              
          }
          else if(isset($result['access_token'])){

              //API Call for getting website INFO
              $inputs = array();
              $headers = array('authorization'=>"bearer ".$result['access_token']);
              $website = $apiObj->call(BASEURL.get_option('SITE_CODE'),"GET",$inputs,$headers);

             // write_log($website['result']['locations']);

              for($i=0;$i<count($website['result']['locations']);$i++){

                  if($website['result']['locations'][$i]['type'] == 'store'){

                      $location_name = isset($website['result']['locations'][$i]['city'])?$website['result']['locations'][$i]['name']:"";

                      $found_post = post_exists($location_name,'','','store-locations');

                          if( $found_post == 0 ){

                                  $array = array(
                                      'post_title' => $location_name,
                                      'post_type' => 'store-locations',
                                      'post_content'  => "",
                                      'post_status'   => 'publish',
                                      'post_author'   => 0,
                                  );
                                  $post_id = wp_insert_post( $array );

                                //  write_log( $location_name.'---'.$post_id);
                                  
                                  update_post_meta($post_id, 'address', $website['result']['locations'][$i]['address']); 
                                  update_post_meta($post_id, 'city', $website['result']['locations'][$i]['city']); 
                                  update_post_meta($post_id, 'state', $website['result']['locations'][$i]['state']); 
                                  update_post_meta($post_id, 'country', $website['result']['locations'][$i]['country']); 
                                  update_post_meta($post_id, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                  if($website['result']['locations'][$i]['forwardingPhone']==''){

                                    update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['phone']);  
                                    
                                  }else{

                                    update_post_meta($post_id, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                  }
                                                                     
                                  update_post_meta($post_id, 'latitude', $website['result']['locations'][$i]['lat']); 
                                  update_post_meta($post_id, 'longitue', $website['result']['locations'][$i]['lng']); 
                                  update_post_meta($post_id, 'monday', $website['result']['locations'][$i]['monday']); 
                                  update_post_meta($post_id, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                  update_post_meta($post_id, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                  update_post_meta($post_id, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                  update_post_meta($post_id, 'friday', $website['result']['locations'][$i]['friday']); 
                                  update_post_meta($post_id, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                  update_post_meta($post_id, 'sunday', $website['result']['locations'][$i]['sunday']); 
                                  
                          }else{

                                    update_post_meta($found_post, 'address', $website['result']['locations'][$i]['address']); 
                                    update_post_meta($found_post, 'city', $website['result']['locations'][$i]['city']); 
                                    update_post_meta($found_post, 'state', $website['result']['locations'][$i]['state']); 
                                    update_post_meta($found_post, 'country', $website['result']['locations'][$i]['country']); 
                                    update_post_meta($found_post, 'postal_code', $website['result']['locations'][$i]['postalCode']); 
                                    if($website['result']['locations'][$i]['forwardingPhone']==''){

                                    update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['phone']);  
                                    
                                    }else{

                                    update_post_meta($found_post, 'phone', $website['result']['locations'][$i]['forwardingPhone']);  
                                    }
                                                                    
                                    update_post_meta($found_post, 'latitude', $website['result']['locations'][$i]['lat']); 
                                    update_post_meta($found_post, 'longitue', $website['result']['locations'][$i]['lng']); 
                                    update_post_meta($found_post, 'monday', $website['result']['locations'][$i]['monday']); 
                                    update_post_meta($found_post, 'tuesday', $website['result']['locations'][$i]['tuesday']); 
                                    update_post_meta($found_post, 'wednesday', $website['result']['locations'][$i]['wednesday']); 
                                    update_post_meta($found_post, 'thursday', $website['result']['locations'][$i]['thursday']); 
                                    update_post_meta($found_post, 'friday', $website['result']['locations'][$i]['friday']); 
                                    update_post_meta($found_post, 'saturday', $website['result']['locations'][$i]['saturday']); 
                                    update_post_meta($found_post, 'sunday', $website['result']['locations'][$i]['sunday']); 

                          }

                  }
              
              }

          }    

}



//get ipaddress of visitor

function getUserIpAddr() {
  $ipaddress = '';
  if (isset($_SERVER['HTTP_CLIENT_IP']))
      $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
  else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
  else if(isset($_SERVER['HTTP_X_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
  else if(isset($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']))
      $ipaddress = $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
  else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
      $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
  else if(isset($_SERVER['HTTP_FORWARDED']))
      $ipaddress = $_SERVER['HTTP_FORWARDED'];
  else if(isset($_SERVER['REMOTE_ADDR']))
      $ipaddress = $_SERVER['REMOTE_ADDR'];
  else
      $ipaddress = 'UNKNOWN';
  
  if ( strstr($ipaddress, ',') ) {
          $tmp = explode(',', $ipaddress,2);
          $ipaddress = trim($tmp[1]);
  }
  return $ipaddress;
}


// Custom function for lat and long

function get_storelisting() {
  
  global $wpdb;
  $content="";


//  echo 'no cookie';
  
  $urllog = 'https://global-ds.cloud.netacuity.com/webservice/query';
  //12.68.65.195
  $response = wp_remote_post( $urllog, array(
      'method' => 'GET',
      'timeout' => 45,
      'redirection' => 5,
      'httpversion' => '1.0',
      'headers' => array('Content-Type' => 'application/json'),         
      'body' =>  array('u'=> 'b25d7667-74cc-4fcc-9adf-7b5f4f8f5bd0','ip'=> getUserIpAddr(),'dbs'=> 'all','trans_id'=> 'example','json'=> 'true' ),
      'blocking' => true,               
      'cookies' => array()
      )
  );
      
      $rawdata = json_decode($response['body'], true);
      $userdata = $rawdata['response'];

      $autolat = $userdata['pulseplus-latitude'];
      $autolng = $userdata['pulseplus-longitude'];
      if(isset($_COOKIE['preferred_store'])){

          $store_location = $_COOKIE['preferred_store'];
      }else{

          $store_location = '';
         
        }

      //print_r($rawdata);
      
      $sql =  "SELECT post_lat.meta_value AS lat,post_lng.meta_value AS lng,posts.ID, 
              ( 3959 * acos( cos( radians(".$autolat." ) ) * cos( radians( post_lat.meta_value ) ) * 
              cos( radians( post_lng.meta_value ) - radians( ".$autolng." ) ) + sin( radians( ".$autolat." ) ) * 
              sin( radians( post_lat.meta_value ) ) ) ) AS distance FROM wp_posts AS posts
              INNER JOIN wp_postmeta AS post_lat ON post_lat.post_id = posts.ID AND post_lat.meta_key = 'latitude'
              INNER JOIN wp_postmeta AS post_lng ON post_lng.post_id = posts.ID AND post_lng.meta_key = 'longitue'  
              WHERE posts.post_type = 'store-locations' 
              AND posts.post_status = 'publish' GROUP BY posts.ID HAVING distance < 50000000000000000000 ORDER BY distance";

            //  write_log($sql);

      $storeposts = $wpdb->get_results($sql);
      $storeposts_array = json_decode(json_encode($storeposts), true);      

     
      if($store_location ==''){
          //write_log('empty loc');           
          $store_location = $storeposts_array['0']['ID'];
          $store_distance = round($storeposts_array['0']['distance'],1);
      }else{

         // write_log('noempty loc');
          $key = array_search($store_location, array_column($storeposts_array, 'ID'));
          $store_distance = round($storeposts_array[$key]['distance'],1);           

      }
      
    
      $content = get_the_title($store_location); 

  foreach ( $storeposts as $post ) {

    //write_log(get_field('phone', $post->ID));
   
                $content_list .= '<div class="location-section" id ="'.$post->ID.'">
                <div class="location-info-section">
                    <div class="location-name">'.get_the_title($post->ID).' - <b>'.round($post->distance,1).' MI</b></div>
                    <p class="store-add"> '.get_field('address', $post->ID).'<br />'.get_field('city',$post->ID).', '.get_field('state',$post->ID).' '.get_field('postal_code', $post->ID).'</p>
                    <p class="store-phone"><i class="fa fa-phone-alt"></i> &nbsp;'.get_field('phone', $post->ID).'</p>
                    <div class="store-hour-section">
                        <div class="label">STORE HOURS</div>
                        <ul class="storename"><li><div class="store-opening-hrs-container">
                        <ul class="store-opening-hrs">
                            <li>Monday: <span>'.get_field('monday', $post->ID).'</span></li>
                            <li>Tuesday: <span>'.get_field('tuesday', $post->ID).'</span></li>
                            <li>Wednesday: <span>'.get_field('wednesday', $post->ID).'</span></li>
                            <li>Thursday: <span>'.get_field('thursday', $post->ID).'</span></li>
                            <li>Friday: <span>'.get_field('friday', $post->ID).'</span></li>
                            <li>Saturday: <span>'.get_field('saturday', $post->ID).'</span></li>
                            <li>Sunday: <span>'.get_field('sunday', $post->ID).'</span></li>
                        </ul>
                        </div></li></ul>
                    </div>
                    '.do_shortcode('[storelocation_address "dir" "'.get_the_title($post->ID).'"]').'
                    <a class="links" href="/request-appointment/">REQUEST APPOINTMENT</a>
                    
                    <a href="javascript:void(0)" data-id="'.$post->ID.'" data-storename="'.get_the_title($post->ID).'" data-distance="'.round($post->distance,1).'" target="_self" class="store-cta-link choose_location">Choose This Location</a>
                </div>
            </div>'; 
  } 
  

  $data = array();

  $data['header']= $content;
  $data['list']= $content_list;

 // write_log($data['list']);

  echo json_encode($data);
      wp_die();
}

//add_shortcode('storelisting', 'get_storelisting');
add_action( 'wp_ajax_nopriv_get_storelisting', 'get_storelisting', '1' );
add_action( 'wp_ajax_get_storelisting', 'get_storelisting','1' );



//choose this location FUnctionality

add_action( 'wp_ajax_nopriv_choose_location', 'choose_location' );
add_action( 'wp_ajax_choose_location', 'choose_location' );

function choose_location() {     
      
      $content = get_the_title($_POST['store_id']) ; 

      echo $content;

  wp_die();
}

//   function my_after_header() {
//       echo '<div class="header_store"></div><div id="ajaxstorelisting"> </div>
//       <div class="locationOverlay"></div>';
//     }
//     add_action( 'fl_after_header', 'my_after_header' );

function location_header() {
    return '<div class="header_store"></div><div id="ajaxstorelisting"> </div>
    <div class="locationOverlay"></div>';
  }
add_shortcode('location_code', 'location_header');

add_filter( 'gform_pre_render_15', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_15', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_15', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_15', 'populate_product_location_form' );

add_filter( 'gform_pre_render_25', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_25', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_25', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_25', 'populate_product_location_form' );

add_filter( 'gform_pre_render_21', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_21', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_21', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_21', 'populate_product_location_form' );

add_filter( 'gform_pre_render_17', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_17', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_17', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_17', 'populate_product_location_form' );

add_filter( 'gform_pre_render_23', 'populate_product_location_form' );
add_filter( 'gform_pre_validation_23', 'populate_product_location_form' );
add_filter( 'gform_pre_submission_filter_23', 'populate_product_location_form' );
add_filter( 'gform_admin_pre_render_23', 'populate_product_location_form' );


// add_filter( 'gform_pre_render_23', 'populate_product_location_form' );
// add_filter( 'gform_pre_validation_35', 'populate_product_location_form' );
// add_filter( 'gform_pre_submission_filter_35', 'populate_product_location_form' );
// add_filter( 'gform_admin_pre_render_35', 'populate_product_location_form' );

function populate_product_location_form( $form ) {

foreach ( $form['fields'] as &$field ) {

    // Only populate field ID 12
    if ( $field->type != 'select' || strpos( $field->cssClass, 'populate-store' ) === false ) {
        continue;
    }      	

        $args = array(
            'post_type'      => 'store-locations',
            'posts_per_page' => -1,
            'post_status'    => 'publish'
        );										
      
         $locations =  get_posts( $args );

         $choices = array(); 

         foreach($locations as $location) {
            

              $title = get_the_title($location->ID);
              
                   $choices[] = array( 'text' => $title, 'value' => $title );

          }
          wp_reset_postdata();

         // write_log($choices);

         // Set placeholder text for dropdown
         $field->placeholder = '-- Choose Location --';

         // Set choices from array of ACF values
         $field->choices = $choices;
    
}
return $form;
}

add_action( 'fl_before_post_content', function(){
    if ( is_front_page() && is_home() ){
        // Default homepage
    } elseif ( is_front_page()){
        //Static homepage
    } elseif ( is_home() || is_category()){
        ?>
    <header class="fl-post-header fl-post-custom-header">
        <h2 class="fl-post-title" itemprop="headline">
            <a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a>
            <?php edit_post_link( _x( 'Edit', 'Edit post link text.', 'fl-automator' ) ); ?>
        </h2>
        <?php FLTheme::post_top_meta(); ?>
    </header>
<?php 
    } else{
        //everything else
    }
    
} );

//Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_100012_override_yoast_breadcrumb_trail_new' );

function wpse_100012_override_yoast_breadcrumb_trail_new( $links ) {
    global $post;

    if ( is_singular( 'hardwood_catalog' )  ) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood-flooring/',
            'text' => 'Hardwood',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/hardwood-flooring/browse-hardwood-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );

    }elseif (is_singular( 'carpeting' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpeting/',
            'text' => 'Carpet',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/carpeting/browse-carpeting-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );

    }elseif (is_singular( 'luxury_vinyl_tile' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/vinyl/',
            'text' => 'Vinyl',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/vinyl/browse-vinyl-flooring-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }elseif (is_singular( 'laminate_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate/',
            'text' => 'Laminate',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/laminate/browse-laminate-flooring-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }elseif (is_singular( 'tile_catalog' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/ceramic-tile-floors/',
            'text' => 'Ceramic Tile',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/ceramic-tile-floors/browse-ceramic-tile-flooring-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -1, $breadcrumb );
        
    }elseif (is_singular( 'solid_wpc_waterproof' )) {

        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/',
            'text' => 'Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/waterproof-flooring/',
            'text' => 'Waterproof Flooring',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/flooring/waterproof-flooring/browse-waterproof-flooring-catalog/',
            'text' => 'Products',
        );
        array_splice( $links, 1, -2, $breadcrumb );
    }

    return $links;
}


//301 redirect added from 404 logs table
wp_clear_scheduled_hook( '404_redirection_301_log_cronjob' );
if (!wp_next_scheduled('floorstore_404_redirection_301_log_cronjob')) {
    
    $interval =  getIntervalTime('friday');    
    wp_schedule_event( time() +  17800, 'daily', 'floorstore_404_redirection_301_log_cronjob');
}

add_action( 'floorstore_404_redirection_301_log_cronjob', 'floorstore_custom_404_redirect_hook' ); 



function floorstore_check_404($url) {
   $headers=get_headers($url, 1);
   if ($headers[0]!='HTTP/1.1 200 OK') {return true; }else{ return false;}
}

 // custom 301 redirects from  404 logs table
 function floorstore_custom_404_redirect_hook(){
    global $wpdb;    
    write_log('in function');

    $table_redirect = $wpdb->prefix.'redirection_items';
    $table_name = $wpdb->prefix . "redirection_404";
    $table_group = $wpdb->prefix.'redirection_groups';

     $data_404 = $wpdb->get_results( "SELECT * FROM $table_name" );
     $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
     $redirect_group =  $datum[0]->id;  

    if ($data_404)
    {      
      foreach ($data_404 as $row_404) 
       {            

        if (strpos($row_404->url,'carpet') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {
            write_log($row_404->url);      
            
           $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = floorstore_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

           
            $destination_url_carpet = '/flooring/carpeting/browse-carpeting-catalog/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_carpet,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            write_log( 'carpet 301 added ');
         }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');


         }

        }
        else if (strpos($row_404->url,'hardwood') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');
            $url_404 = home_url().''.$row_404->url;
            $headers_res = floorstore_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            $destination_url_hardwood = '/flooring/hardwood-flooring/browse-hardwood-catalog/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_hardwood,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }

            write_log( 'hardwood 301 added ');

        }
        else if (strpos($row_404->url,'laminate') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = floorstore_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            $destination_url_laminate = '/flooring/laminate/browse-laminate-flooring-catalog/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" =>$row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_laminate,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'laminate 301 added ');


        }
        else if ((strpos($row_404->url,'luxury-vinyl') !== false || strpos($row_404->url,'vinyl') !== false) && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);  

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = floorstore_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            if (strpos($row_404->url,'luxury-vinyl') !== false ){
                $destination_url_lvt = '/flooring/vinyl/browse-vinyl-flooring-catalog/';
            }elseif(strpos($row_404->url,'vinyl') !== false){
                $destination_url_lvt = '/flooring/';
            }
            
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" =>$row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_lvt,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
            }

            write_log( 'luxury-vinyl 301 added ');
        }
        else if (strpos($row_404->url,'ceramic-tile') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = floorstore_check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $destination_url_tile = '/flooring/ceramic-tile-floors/browse-ceramic-tile-flooring-catalog/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_tile,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'tile 301 added ');
        }

       }  
    }

 }
 //add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}

add_filter( 'auto_update_plugin', '__return_false' );


//Accessibility employee shortcode
function accessibility_shortcodes($arg) {  

    $website_json =  json_decode(get_option('website_json'));
     

     foreach($website_json->contacts as $contact){

        if($contact->accessibility == '1'){ 

            if (in_array("phone", $arg)) {

                $content = '<a href="tel:'.$contact->phone.'">'.$contact->phone.'</a>';                
               
            }
            if (in_array("email", $arg)) {

                $content = '<a href="mailto:'.$contact->email.'">'.$contact->email.'</a>';              
               
            }
        }
    }

    return  $content;
    
}
add_shortcode('accessibility', 'accessibility_shortcodes');

add_filter( 'facetwp_pager_html', function( $output, $params ) {
    $output = '';
    $page = $params['page'];
    $total_pages = $params['total_pages'];
    global $wp;

    $url_args = "";
    
    if(count($_GET) > 0){ 
        foreach($_GET as $key => $value){
            if($key !="fwp_paged"){
                $url_args.= $key."=".$value."&";
            }
        }
    }
    $current_url = home_url($wp->request)."?".$url_args;
    if ( 1 < $total_pages ) {

        // Previous page (NEW)
        if ( $page > 1 ) {
            $output .= '<a class="facetwp-page" data-page="' . ($page - 1) . '" href="'.$current_url.'fwp_paged='.($page-1).'">Previous</a>';
        }
        
        if ( 3 < $page ) {
            $output .= '<a class="facetwp-page first-page" data-page="1"  href="'.$current_url.'fwp_paged=1"><<</a>';
        }
        // if ( 1 < ( $page - 10 ) ) {
        //     $output .= '<a class="facetwp-page" data-page="' . ($page - 10) . '">' . ($page - 10) . '</a>';
        // }
        for ( $i = 2; $i > 0; $i-- ) {
            if ( 0 < ( $page - $i ) ) {
                $output .= '<a class="facetwp-page" data-page="' . ($page - $i) . '"  href="'.$current_url.'fwp_paged='.($page-$i).'">' . ($page - $i) . '</a>';
            }
        }

        // Current page
        $output .= '<a class="facetwp-page active" data-page="' . $page . '">' . $page . '</a>';

        for ( $i = 1; $i <= 2; $i++ ) {
            if ( $total_pages >= ( $page + $i ) ) {
                $output .= '<a class="facetwp-page" data-page="' . ($page + $i) . '" href="'.$current_url.'fwp_paged='.($page+$i).'">' . ($page + $i) . '</a>';
            }
        }
        // if ( $total_pages > ( $page + 10 ) ) {
        //     $output .= '<a class="facetwp-page" data-page="' . ($page + 10) . '">' . ($page + 10) . '</a>';
        // }
        if ( $total_pages > ( $page + 2 ) ) {
            $output .= '<a class="facetwp-page last-page" data-page="' . $total_pages . '" href="'.$current_url.'fwp_paged='.($total_pages).'">>></a>';
        }

        // Next page (NEW)
        if ( $page < $total_pages ) {
            $output .= '<a class="facetwp-page" data-page="' . ($page + 1) . '" href="'.$current_url.'fwp_paged='.($page + 1).'">Next</a>';
        }
    }

    return $output;
}, 10, 2 );