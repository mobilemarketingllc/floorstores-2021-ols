<?php 
/*
Plugin Name: Grandchild Theme
Plugin URI: http://www.wp-code.com/
Description: Retailer Setting Plugin
Author: MM
Version: 9.99999
Author URI: https://wpmaster.mm-dev.agency
*/

include( dirname( __FILE__ ) . '/include/apicaller.php' );
include( dirname( __FILE__ ) . '/include/constant.php' );
include( dirname( __FILE__ ) . '/include/helper.php' );
include( dirname( __FILE__ ) . '/include/track-leads.php' );    

/** GTM */
include( dirname( __FILE__ ) . '/include/gtm-manager.php' ); 

include( dirname( __FILE__ ) . '/velocity-sales.php' );   

require_once( ABSPATH . "wp-includes/pluggable.php" );
require_once( ABSPATH . "/wp-load.php" );


require_once plugin_dir_path( __FILE__ ) . 'example-plugin.php';
new Example_Background_Processing();
     


$module_update =1;
$product_check_table = $wpdb->prefix."product_check";
$product_sync_table = $wpdb->prefix."sfn_sync";
$posts_table = $wpdb->prefix."posts";
$postmeta_table = $wpdb->prefix."postmeta";



/** Theme global js **/
function wpc_theme_global() {
    
    define( 'postpercol', '3' );
    include( dirname( __FILE__ ) . '/styles.php' );
    // define( 'productdetail_layout', 'box' );    
    define( 'productdetail_layout', 'box' );  
    wp_enqueue_style('lightbox-style', plugins_url('css/lightgallery.min.css', __FILE__));
    wp_enqueue_script('lightbox-js',plugins_url( 'js/lightgallery-all.min.js', __FILE__));
    wp_enqueue_script('script-js',plugins_url( 'js/script.min.js', __FILE__));
    gtm_head_script();
}
add_action('wp_head', 'wpc_theme_global');
 
function admin_style() {
    wp_enqueue_style('admin-styles', plugins_url('css/admin.min.css', __FILE__));
    wp_enqueue_script('script',plugins_url( '/js/retailer_v1.min.js', __FILE__ ));
}
add_action('admin_enqueue_scripts', 'admin_style');
// This filter replaces a complete file from the parent theme or child theme with your file (in this case the archive page).
// Whenever the archive is requested, it will use YOUR archive.php instead of that of the parent or child theme.
//add_filter ('archive_template', create_function ('', 'return plugin_dir_path(__FILE__)."archive.php";'));

function wpc_theme_add_headers () {
    wp_enqueue_style('font-awesome-styles', plugins_url('css/font-awesome.min.css', __FILE__));
    wp_enqueue_style('frontend-styles', plugins_url('css/styles.min.css', __FILE__));
}
// These two lines ensure that your CSS is loaded alongside the parent or child theme's CSS
add_action('init', 'wpc_theme_add_headers');
 
// In the rest of your plugin, add your normal actions and filters, just as you would in functions.php in a child theme.

function get_custom_post_type_template($single_template) {
    global $post;
      
    if ($post->post_type != 'post') {
         $single_template = dirname( __FILE__ ) . '/product-listing-templates/single-'.$post->post_type.'.php';
    }
    return $single_template;
}

if(get_option('is_own_templates') !='1'){
  
    add_filter( 'single_template', 'get_custom_post_type_template' );
}

//Code for adding Menu
//Nikhil Chinchane: 7 Jan 2019
//
function add_my_menu() {
    add_menu_page (
        'Retailer Settings', // page title 
        'Retailer Settings', // menu title
        'manage_options', // capability
        'my-menu-slug',  // menu-slug
        'my_menu_page',   // function that will render its output
        plugins_url('/img/theme-option-menu-icon.png', __FILE__)   // link to the icon that will be displayed in the sidebar
        //$position,    // position of the menu option
    );

    add_submenu_page('my-menu-slug', __('Retailer Product Data'), __('Retailer Product Data'), 'manage_options', 'retailer_product_data', 'retailer_product_data_html');
    add_submenu_page('my-menu-slug', __('Retailer Promotion'), __('Retailer Promotion'), 'manage_options', 'promotion', 'promotion_func');
    add_submenu_page('my-menu-slug', __('Documentation'), __('Documentation'), 'manage_options', 'documentation', 'documentation_func');
   
}
add_action('admin_menu', 'add_my_menu');

function changeTimeZone($dateString, $timeZoneSource = null, $timeZoneTarget = null)
{
  if (empty($timeZoneSource)) {
    $timeZoneSource = date_default_timezone_get();
  }
  if (empty($timeZoneTarget)) {
    $timeZoneTarget = date_default_timezone_get();
  }

  $dt = new DateTime($dateString, new DateTimeZone($timeZoneSource));
  $dt->setTimezone(new DateTimeZone($timeZoneTarget));

  return $dt->format("Y-m-d H:i:s a");
}

function overwritemodeules(){
    global $module_update;
    // echo $wp_root_path = str_replace('/wp-content/themes', '', get_theme_root());echo "<br>";
    
    $theme_directory = get_template_directory()."-child";echo "<br>";///var/www/html/wp-content/themes/astra-child
    $plugin_directory = plugin_dir_path( __FILE__ );///var/www/html/wp-content/plugins/grand-child/
    
    //check child directory is created for not
    
    if (is_dir($theme_directory)){
        // Check fl-builder modules is exist for not        '
        
        if(is_dir($theme_directory ."/fl-builder")){
            
            rename($theme_directory ."/fl-builder",$theme_directory ."/fl-builder".time());
            var_dump(xcopy($plugin_directory ."fl-builder",$theme_directory ."/fl-builder"));
        }
        else{
            echo "NOT EXIST";
            //var_dump(xcopy("/var/www/html/wp-content/plugins/grand-child/fl-builder","/var/www/html/wp-content/themes/bb-theme-child/fl-builder"));
            //echo $plugin_directory ."fl-builder",$theme_directory."/fl-builder";exit;
            
                //var_dump(xcopy($plugin_directory ."fl-builder",get_template_directory()."/fl-builder"));
                var_dump(xcopy($plugin_directory ."fl-builder",$theme_directory ."/fl-builder"));
            
        }
    }
     else{

        if (is_dir( get_template_directory()."/fl-builder")) {
            rename(get_template_directory()."/fl-builder",get_template_directory() ."/fl-builder".time());
            var_dump(xcopy($plugin_directory ."fl-builder",get_template_directory()."/fl-builder"));
        }
        else{
            var_dump(xcopy($plugin_directory ."fl-builder",get_template_directory()."/fl-builder"));
        }
         
        //mkdir($theme_directory);
        
    }
    if(get_site_option( 'overwrite_module' ) !== false ){
        update_option( 'overwrite_module', $module_update );
    }
    else{
        
        add_option( 'overwrite_module', $module_update );
    }
}

add_action( 'plugins_loaded', 'overwritemodulesintheme' );
function overwritemodulesintheme() {
    
    
    global $module_update;
    if(get_site_option( 'overwrite_module' ) !== false ){
        if ( get_site_option( 'overwrite_module' ) != $module_update ) {
            overwritemodeules();
           
        }
    }
    else{
        
        overwritemodeules();
    }
    
    
}


function  query_group_by_filter($groupby){
    global $wpdb;

    return $wpdb->postmeta . '.meta_value ';
 }

add_filter( 'facetwp_search_query_args', function( $search_args, $params ) {
    remove_filter('posts_groupby', 'query_group_by_filter'); 
    return $search_args;
}, 10, 2 );

add_filter( 'facetwp_result_count', function( $output, $params ) {
    $output = $params["lower"]."-".$params["upper"]." of ".$params['total'] .' Results';
    return $output;
}, 10, 2 );


add_filter( 'facetwp_facet_render_args', function( $args) {
    global $post;
	if ( 'search' == $args['facet']['name'] ) { 
        $current = $post->ID;
        $parent = $post->post_parent;
		$args['facet']['placeholder'] = 'Search '.get_the_title($parent);
	}
	return $args;
} );

add_filter( 'facetwp_template_html', function( $output, $class ) {
    //$GLOBALS['wp_query'] = $class->query;
    $prod_list = $class->query;
    
   ob_start();
    

   if( get_option('plplayout') !='' && get_option('plplayout') == '1' ){

        $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/product-loop-popup.php';
        require_once $dir;
   }else{
        $dir = WP_PLUGIN_DIR.'/grand-child/product-listing-templates/product-loop-new.php';
        require_once $dir;
   }  
    
    return ob_get_clean();
}, 10, 2 );


add_action( 'sale_hook_new', 'sale_api_call' );
add_action( 'init', 'single_event');

function single_event(){
    if( !wp_next_scheduled( 'sale_hook_new' ) ) {
        // Schedule the event
        wp_schedule_single_event( time()+600, "sale_hook_new",array("2")); 
        
    }
}

function sale_api_call(){
    $apiObj = new APICaller;
    $inputs = array('grant_type'=>'client_credentials','client_id'=>get_option('CLIENT_CODE'),'client_secret'=>get_option('CLIENTSECRET'));
    $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);
    
    
    if(isset($result['error'])){
        $msg =$result['error'];                
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] =$result['error_description'];
        
    }
    else if(isset($result['access_token'])){
        callSaleAPI($apiObj,$result);
        sync_websiteinfo_cde($apiObj,$result);
        
    }
     //Change for daily one sync mail
     $time = date("H");
     if($time >= 1 && $time < 2){
     //wp_mail( 'ram@mobile-marketing.agency', 'SINGLE EVENT  Sale API Called for '. get_bloginfo(), 'SINGLE EVENT '.date("Y-m-d h:i:s",time()) );
     }
}


function getIntervalTime($day){

    date_default_timezone_set("Asia/Kolkata");
     $time = (int)time();  
    $daytime = (int)strtotime('next '.$day); 
    return  ($daytime +34200) - $time;
   
}


/**  Cron job timing **/

add_filter( 'cron_schedules', 'example_add_cron_interval' );

function example_add_cron_interval( $schedules ) 
{ 
    
    $schedules['one_min'] = array(
        'interval' => 3600,
        'display' => esc_html__( 'Every one hour' ),
    );
    $schedules['each_monday'] = array(
        'interval' => getIntervalTime('monday'),
        'display' => esc_html__( 'Each Monday' ),
    );
    $schedules['each_tuesday'] = array(
        'interval' => getIntervalTime('tuesday'),
        'display' => esc_html__( 'Each Tuesday' ),
    );
    $schedules['each_wednesday'] = array(
        'interval' => getIntervalTime('wednesday'),
        'display' => esc_html__( 'Each Wednesday' ),
    );
    $schedules['each_thursday'] = array(
        'interval' => getIntervalTime('thursday'),
        'display' => esc_html__( 'Each Thursday' ),
    );
    $schedules['each_friday'] = array(
        'interval' => getIntervalTime('friday'),
        'display' => esc_html__( 'Each Friday' ),
    );
    $schedules['each_saturday'] = array(
        'interval' => getIntervalTime('saturday'),
        'display' => esc_html__( 'Each Saturday' ),
    );
    $schedules['each_sunday'] = array(
        'interval' => getIntervalTime('sunday'),
        'display' => esc_html__( 'Each Sunday' ),
    );

    return $schedules;
}

/**  Blog sync Function **/

function blog_function() {

    $apiObj = new APICaller;
    $inputs = array('grant_type'=>'client_credentials','client_id'=>get_option('CLIENT_CODE'),'client_secret'=>get_option('CLIENTSECRET'));
    $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);
    
    
    if(isset($result['error'])){
        $msg =$result['error'];                
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] =$result['error_description'];
        
    }
    else if(isset($result['access_token'])){
        callBlogpostAPI($apiObj,$result);
    }
//wp_mail( 'devteam.agency@gmail.com', 'Blog API Called for '. get_bloginfo(), 'WP cron run at '.date("Y-m-d h:i:s",time()) );
}


add_action( 'CDE_blog_sync', 'blog_function' );

function CDE_sync_function() {

            //updated fields for block country

            global $wpdb;
            $redirect_table = $wpdb->prefix.'redirection_items';
            
            $data_serial = array( 'CA', 'US', 'UM' );     
            update_option( 'blockcountry_backendbanlist', $data_serial);
            update_option( 'blockcountry_banlist', $data_serial);
            update_option( 'blockcountry_backendbanlist_inverse', 'on');
            update_option( 'blockcountry_banlist_inverse', 'on');
            update_option( 'blockcountry_frontendwhitelist', '45.55.43.154;114.143.174.138;1.22.231.34/32;');
            update_option( 'blockcountry_backendwhitelist', '54.191.137.17;114.143.174.138;1.22.231.34/32;');

            $wpdb->query('delete from '.$redirect_table.' where id in (Select id from (SELECT action_data , CONCAT(match_url  ,"/") as url , id FROM '.$redirect_table.'  as t) as p  where p.action_data= p.url )');
            $wpdb->query('delete from '.$redirect_table.' where id in (Select id from (SELECT action_data , CONCAT(match_url  ,"") as url , id FROM '.$redirect_table.'  as t) as p  where p.action_data= p.url )');
 

            $apiObj = new APICaller;
            $inputs = array('grant_type'=>'client_credentials','client_id'=>get_option('CLIENT_CODE'),'client_secret'=>get_option('CLIENTSECRET'));
            $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);
            
            
            if(isset($result['error'])){
                $msg =$result['error'];                
                $_SESSION['error'] = $msg;
                $_SESSION["error_desc"] =$result['error_description'];
                
            }
            else if(isset($result['access_token'])){
                callSaleAPI($apiObj,$result);
                sync_websiteinfo_cde($apiObj,$result);
                get_option('CDE_LAST_SYNC_TIME')?update_option('CDE_LAST_SYNC_TIME',time()):add_option('CDE_LAST_SYNC_TIME',time()); 
                
            }
    //wp_mail( 'ram@mobile-marketing.agency', 'Sale API Called for '. get_bloginfo(), 'WP cron run at '.date("Y-m-d h:i:s",time()) );
}
add_action( 'CDE_sync_cronjob', 'CDE_sync_function' );
add_action( 'init', 'register_cron_delete_event');



// Function which will register the event
function register_cron_delete_event() {
    
	// Make sure this event hasn't been scheduled
	if( !wp_next_scheduled( 'CDE_sync_cronjob' ) ) {
        // Schedule the event
        
		wp_schedule_event(time(), 'one_min', 'CDE_sync_cronjob' );
    }
    if( !wp_next_scheduled( 'CDE_blog_sync' ) ) {
        // Schedule the event
        
		wp_schedule_event(time(), 'daily', 'CDE_blog_sync' );
    }
    // Make sure this event hasn't been scheduled
	if( !wp_next_scheduled( 'sfn_bb_autoimport_hook' ) ) {
        // Schedule the event
        
		wp_schedule_event(time(), 'one_min', 'sfn_bb_autoimport_hook' );
    }

    $product_json =  json_decode(get_option('product_json'));     
     $carpet_array = getArrayFiltered('productType','carpet',$product_json);

    // write_log($carpet_array);

    wp_clear_scheduled_hook( 'sync_carpet_monday_event' );

    foreach($carpet_array as $carpet_brand) {   

        if (strtolower($carpet_brand->manufacturer) == 'mohawk'){
                
                if (! wp_next_scheduled ( 'sync_carpet_monday_event_mohawk')) {

                    $interval =  getIntervalTime('monday');      
                    wp_schedule_event( time() + $interval - 10800, 'each_monday', 'sync_carpet_monday_event_mohawk');
                }     

        } else if (strtolower($carpet_brand->manufacturer) == 'shaw'){

                if (! wp_next_scheduled ( 'sync_carpet_tuesday_event_shaw')) {

                    $interval =  getIntervalTime('tuesday');      
                    wp_schedule_event( time() + $interval - 10800, 'each_tuesday', 'sync_carpet_tuesday_event_shaw');
                }

        } else{
            if (! wp_next_scheduled ( 'sync_carpet_saturday_event_dreamweaver')) {

                $interval =  getIntervalTime('saturday');    
                wp_schedule_event( time() + $interval, 'each_saturday', 'sync_carpet_saturday_event_dreamweaver');
            }
        }

    }
   
    wp_clear_scheduled_hook( 'sync_hardwood_tuesday_event' );
    if (! wp_next_scheduled ( 'sync_hardwood_wednesday_event')) {

        $interval =  getIntervalTime('wednesday');    
        wp_schedule_event( time() + $interval, 'each_wednesday', 'sync_hardwood_wednesday_event');
    }     

    wp_clear_scheduled_hook( 'sync_laminate_wednesday_event' );    
    if (! wp_next_scheduled ( 'sync_laminate_thursday_event')) {

        $interval =  getIntervalTime('thursday'); 
        wp_schedule_event(time() + $interval , 'each_thursday', 'sync_laminate_thursday_event');
    }
   
    wp_clear_scheduled_hook( 'special_sync_lvt_thursday_event' );
    //wp_clear_scheduled_hook( 'finalsync_lvt_thursday_event' );    
    if (! wp_next_scheduled ( 'sync_lvt_thursday_event')) {      

        $interval =  getIntervalTime('thursday');    
        wp_schedule_event( time() + $interval + 20800 , 'each_thursday', 'sync_lvt_thursday_event');
    }
    
   // wp_clear_scheduled_hook( 'sync_tile_friday_event' );
    if (! wp_next_scheduled ( 'sync_tile_friday_event')) {

        $interval =  getIntervalTime('friday');    
        wp_schedule_event( time() + $interval + 10800, 'each_friday', 'sync_tile_friday_event');
    }
   
}
register_activation_hook(__FILE__, 'my_activation');

/**  Function for activate cron job  **/
function my_activation() {
    if (! wp_next_scheduled ( 'CDE_sync_cronjob' )) {
	    wp_schedule_event(time(), 'one_min', 'CDE_sync_cronjob');
    }
    if (! wp_next_scheduled ( 'CDE_blog_sync' )) {
	    wp_schedule_event(time(), 'daily', 'CDE_blog_sync');
    }
    if (! wp_next_scheduled ( 'sfn_bb_autoimport_hook' )) {
	    wp_schedule_event(time(), 'one_min', 'sfn_bb_autoimport_hook');
    }
}


register_deactivation_hook(__FILE__, 'my_deactivation');

function my_deactivation() {
	wp_clear_scheduled_hook('my_hourly_event');
}





if(isset($_POST['overright_module']) && $_POST['overright_module'] == 1 ){
   // echo $wp_root_path = str_replace('/wp-content/themes', '', get_theme_root());echo "<br>";
    
    $theme_directory = get_template_directory()."-child";echo "<br>";///var/www/html/wp-content/themes/astra-child
    $plugin_directory = plugin_dir_path( __FILE__ );///var/www/html/wp-content/plugins/grand-child/
    
    //check child directory is created for not
    
    if (is_dir($theme_directory)){
        // Check fl-builder modules is exist for not        '
        
        if(is_dir($theme_directory ."/fl-builder")){
            
            rename($theme_directory ."/fl-builder",$theme_directory ."/fl-builder".time());
            var_dump(xcopy($plugin_directory ."fl-builder",$theme_directory ."/fl-builder"));
        }
        else{
            echo "NOT EXIST";
            //var_dump(xcopy("/var/www/html/wp-content/plugins/grand-child/fl-builder","/var/www/html/wp-content/themes/bb-theme-child/fl-builder"));
            //echo $plugin_directory ."fl-builder",$theme_directory."/fl-builder";exit;
            
                //var_dump(xcopy($plugin_directory ."fl-builder",get_template_directory()."/fl-builder"));
                var_dump(xcopy($plugin_directory ."fl-builder",$theme_directory ."/fl-builder"));
            
        }
    }
     else{

        if (is_dir( get_template_directory()."/fl-builder")) {
            rename(get_template_directory()."/fl-builder",get_template_directory() ."/fl-builder".time());
            var_dump(xcopy($plugin_directory ."fl-builder",get_template_directory()."/fl-builder"));
        }
        else{
            var_dump(xcopy($plugin_directory ."fl-builder",get_template_directory()."/fl-builder"));
        }
         
        //mkdir($theme_directory);
        
    }


    
   
}
     

if(isset($_POST['layoutopotion'])){
    
    (get_option('layoutopotion') !== null)?update_option('layoutopotion',$_POST['layoutopotion']):add_option('layoutopotion',$_POST['layoutopotion']);
    
}

if(isset($_POST['is_own_templates'])){
    
    (get_option('is_own_templates') !== null)?update_option('is_own_templates',$_POST['is_own_templates']):add_option('is_own_templates',$_POST['is_own_templates']);
    
}

if(isset($_POST['sh_get_finance'])){
    
    (get_option('sh_get_finance') !== null)?update_option('sh_get_finance',$_POST['sh_get_finance']):add_option('sh_get_finance',$_POST['sh_get_finance']);
    
}

if(isset($_POST['pdp_get_finance'])){
    
    (get_option('pdp_get_finance') !== null)?update_option('pdp_get_finance',$_POST['pdp_get_finance']):add_option('pdp_get_finance',$_POST['pdp_get_finance']);
    
}

if(isset($_POST['hide_getcoupon_button'])){
    
    (get_option('hide_getcoupon_button') !== null)?update_option('hide_getcoupon_button',$_POST['hide_getcoupon_button']):add_option('hide_getcoupon_button',$_POST['hide_getcoupon_button']);
    
}

if(isset($_POST['getfinancereplace'])){
    
    (get_option('getfinancereplace') !== null)?update_option('getfinancereplace',$_POST['getfinancereplace']):add_option('getfinancereplace',$_POST['getfinancereplace']);
    
}

if(isset($_POST['getfinancetext'])){
    
    (get_option('getfinancetext') !== null)?update_option('getfinancetext',$_POST['getfinancetext']):add_option('getfinancetext',$_POST['getfinancetext']);
    
}

if(isset($_POST['getfinancereplaceurl'])){
    
    (get_option('getfinancereplaceurl') !== null)?update_option('getfinancereplaceurl',$_POST['getfinancereplaceurl']):add_option('getfinancereplaceurl',$_POST['getfinancereplaceurl']);
    
}
if(isset($_POST['plplayout'])){
    
    (get_option('plplayout') !== null)?update_option('plplayout',$_POST['plplayout']):add_option('plplayout',$_POST['plplayout']);
    
}
if(isset($_POST['plpproductimg'])){
    
    (get_option('plpproductimg') !== null)?update_option('plpproductimg',$_POST['plpproductimg']):add_option('plpproductimg',$_POST['plpproductimg']);
    
}
if(isset($_POST['getcouponbtn'])){
    
    (get_option('getcouponbtn') !== null)?update_option('getcouponbtn',$_POST['getcouponbtn']):add_option('getcouponbtn',$_POST['getcouponbtn']);
    
}
if(isset($_POST['getcouponreplace'])){
    
    (get_option('getcouponreplace') !== null)?update_option('getcouponreplace',$_POST['getcouponreplace']):add_option('getcouponreplace',$_POST['getcouponreplace']);
    
}

if(isset($_POST['getcouponreplacetext'])){
    
    (get_option('getcouponreplacetext') !== null)?update_option('getcouponreplacetext',$_POST['getcouponreplacetext']):add_option('getcouponreplacetext',$_POST['getcouponreplacetext']);
    
}
if(isset($_POST['getcouponreplaceurl'])){
    
    (get_option('getcouponreplaceurl') !== null)?update_option('getcouponreplaceurl',$_POST['getcouponreplaceurl']):add_option('getcouponreplaceurl',$_POST['getcouponreplaceurl']);
    
}

if(isset($_POST['siteid']) && $_POST['siteid'] !="" && isset($_POST['clientcode']) && $_POST['clientcode'] !=""  && isset($_POST['clientsecret']) && $_POST['clientsecret'] !=""  )
    { 
        $post_7 = get_post( 445994 ); 
        $actio = 'duplicate';
        
            //CALL Authentication API:
            $apiObj = new APICaller;
            $inputs = array('grant_type'=>'client_credentials','client_id'=>$_POST['clientcode'],'client_secret'=>$_POST['clientsecret']);
            $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);
            
            
            if(isset($result['error'])){
                $msg =$result['error'];                
                $_SESSION['error'] = $msg;
                $_SESSION["error_desc"] =$result['error_description'];
                
            }
            else if(isset($result['access_token'])){

                get_option('CLIENT_CODE')?update_option('CLIENT_CODE',$_POST['clientcode']):add_option('CLIENT_CODE',$_POST['clientcode']);
                get_option('SITE_CODE')?update_option('SITE_CODE',$_POST['siteid']):add_option('SITE_CODE',$_POST['siteid']);
                get_option('CLIENTSECRET')?update_option('CLIENTSECRET',$_POST['clientsecret']):add_option('CLIENTSECRET',$_POST['clientsecret']);
                get_option('ACCESS_TOKEN')?update_option('ACCESS_TOKEN',$result['access_token']):add_option('ACCESS_TOKEN',$result['access_token']);
                get_option('CDE_ENV')?update_option('CDE_ENV',$_POST['instance-select']):add_option('CDE_ENV',$_POST['instance-select']); 
                get_option('CDE_LAST_SYNC_TIME')?update_option('CDE_LAST_SYNC_TIME',time()):add_option('CDE_LAST_SYNC_TIME',time()); 
                //API Call for Social Icon
                $inputs = array();
                $headers = array('authorization'=>"bearer ".$result['access_token']);
                $sociallinks = $apiObj->call(BASEURL.get_option('SITE_CODE')."/".SOCIALURL,"GET",$inputs,$headers);
                $socialreport = $apiObj->call(BASEURL.get_option('SITE_CODE')."/".SOCIALURLREPORT,"GET",$inputs,$headers);

                write_log(BASEURL.get_option('SITE_CODE')."/".SOCIALURLREPORT);
                write_log($socialreport);
                if(isset($sociallinks['success']) && $sociallinks['success'] == 1 ){
                    $social_json =  json_encode($sociallinks['result']);
                    get_option('social_links') || get_option('social_links')==""?update_option('social_links',$social_json):add_option('social_links',$social_json);   
                }
                else{
                    $msg =$sociallinks['message'];                
                    $_SESSION['error'] = "Error SOCIAL LINKS";
                    $_SESSION["error_desc"] =$msg;
                    //echo $msg;
                    
                }

                if(isset($socialreport['success']) && $socialreport['success'] == 1 ){
                    $socialreport_json =  json_encode($socialreport['result']);
                    get_option('social_report') || get_option('social_report')==""?update_option('social_report',$socialreport_json):add_option('social_report',$socialreport_json);   
                }
                
                //API Call for getting website INFO
                $inputs = array();
                $headers = array('authorization'=>"bearer ".$result['access_token']);
                $website = $apiObj->call(BASEURL.get_option('SITE_CODE'),"GET",$inputs,$headers);
                
                
                if(isset($website['success']) && $website['success'] ){

                   $sfn_acc = $website['result']['sfn'];
                   $coretec_color = $website['result']['options']['colorWall'];
                   $covid = $website['result']['options']['covid'];


                    $website_json = json_encode($website['result']);
                    get_option('website_json')?update_option('website_json',$website_json):add_option('website_json',$website_json);
                    update_option('sfn_account',$sfn_acc);
                    update_option('covid',$covid);

                    $website_json_data =  json_decode(get_option('website_json'));

                    foreach($website_json_data->sites as $site_cloud){
            
                        if($site_cloud->instance == 'prod'){
                
                            if( $site_cloud->cloudinary == 'true'){
            
                                update_option('cloudinary','true');
            
                            }else{
            
                                update_option('cloudinary','false');
                            }
                           
            
                        }
                    }

                   

                    if ( ! function_exists( 'post_exists' ) ) {
                        require_once( ABSPATH . 'wp-admin/includes/post.php' );
                    }
                    
                    
                    for($i=0;$i<count($website['result']['locations']);$i++){
                        $location_name = isset($website['result']['locations'][$i]['name'])?$website['result']['locations'][$i]['name']:"";

                        if(post_exists($location_name) == 0 && $location_name != "" ){

                            $array = array(
                                'post_title' => $location_name,
                                'post_type' => 'store-locations',
                                'post_content'  => "LOCATIONS",
                                'post_status'   => 'publish',
                                'post_author'   => 0,
                            );
                          //  $post_id = wp_insert_post( $array );
                            
                            
                        }
                    
                    }

                    if(isset($website['result']['sites'])){
                        for($k=0;$k<count($website['result']['sites']);$k++){
                            if($website['result']['sites'][$k]['instance'] == ENV){
                                update_option('blogname',$website['result']['sites'][$k]['name']);
                                $gtmid = $website['result']['sites'][$k]['gtmId'];
                                get_option( 'gtm_script_insert')?update_option( 'gtm_script_insert',$gtmid):update_option( 'gtm_script_insert',$gtmid);
                            }
                        }
                    }
                    //get_option( 'gtm_script_insert', )
                    
                }
                else{
                    $msg =$website['message'];                
                    //echo $msg;
                }
                //API Call for getting Contact INFO
                $inputs = array();
                $headers = array('authorization'=>"bearer ".$result['access_token']);
                $contacts = $apiObj->call(BASEURL.CONTACTURL.get_option('SITE_CODE'),"GET",$inputs,$headers);
                
                if(isset($contacts['success']) && $contacts['success'] ){

                    $contacts_json = json_encode($contacts['result']);
                    get_option('contacts_json')?update_option('contacts_json',$contacts_json):add_option('contacts_json',$contacts_json);
                    
                    $phone = isset($contacts['result'][0]['phone'])?$contacts['result'][0]['phone']:'';
                    
                    get_option('api_contact_phone')?update_option('api_contact_phone',$phone):update_option('api_contact_phone',$phone);
                
                }
                else{
                    $msg =$contacts['message'];                
                    $_SESSION['error'] = "Error Contact Info";
                    $_SESSION["error_desc"] =$msg;
                }
                

                //API Call For getting Promos Information
                //PROMOSAPI
                callSaleAPI($apiObj,$result);
               
                //API Call for geting product brand details:
                    
                $inputs = array();
                $headers = array('authorization'=>"bearer ".$result['access_token']);
                $products = $apiObj->call(SOURCEURL.get_option('SITE_CODE')."/".PRODUCTURL,"GET",$inputs,$headers);
                
           
                if(isset($products) ){

                    $product_json = json_encode($products);
                    get_option('product_json')?update_option('product_json',$product_json):add_option('product_json',$product_json);
                }
                else{
                    $msg =$contacts['message'];                
                    $_SESSION['error'] = "Product Brand API";
                    $_SESSION["error_desc"] =$msg;
                }

                //API Call for geting site blog post:

            }

          
            
           
    }
    else{
        $msg = "Please fill all fields";
        //echo $msg;
    }

    if(isset($_POST['blogsync'])){

            $apiObj = new APICaller;
            $inputs = array('grant_type'=>'client_credentials','client_id'=>get_option('CLIENT_CODE'),'client_secret'=>get_option('CLIENTSECRET'));
           // echo AUTHURL;
            $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);
          // print_r($result);
        callBlogpostAPI($apiObj,$result);
    }


    function callBlogpostAPI($apiObj,$result){

        //write_log('here');
                $inputs = array();
                $headers = array('authorization'=>"bearer ".$result['access_token']);
                $blogposts = $apiObj->call(BLOGURL.get_option('SITE_CODE')."/","GET",$inputs,$headers);               

               
                if(isset( $blogposts['result'])){

                    $blog_json = json_encode($blogposts['result']);
                    get_option('blogpost_json')?update_option('blogpost_json',$blog_json):add_option('blogpost_json',$blog_json);

                }
               
                foreach($blogposts['result'] as $blog_post){

                    $site_post = '';
                   
                   $date_stamp = strtotime($blog_post['startDate']);

                   $current_date = date( 'Y-m-d' );
                   write_log($current_date);
                   $postdate = date("Y-m-d H:i:s", $date_stamp);
                   $post_sync_date = date("Y-m-d", $date_stamp);
                   write_log($post_sync_date);

                    $idObj = get_category_by_slug( 'blog' );
 
                        if ( $idObj) {
                            $blogid = $idObj->term_id;
                        }
                         
                        $the_slug = $blog_post['url'];
                        $args = array(
                        'name'        => $the_slug,
                        'title'  => str_replace('&', '&amp;', $blog_post['name']),
                        'post_type'   => 'post',
                        'post_status' => 'publish',
                        'numberposts' => 1
                        );

                        $my_posts = get_posts($args);

                        //write_log($my_posts);

                        if( $my_posts ){

                            write_log('in if condition - Post already exists - Update Content');

                            $my_post = array(
                                'ID'           => $my_posts[0]->ID,                                
                                'post_content' => $blog_post['text'],
                                'post_excerpt'  => wp_trim_words( $blog_post['text'], 50 )
                            );
                          
                          // Update the post into the database
                            wp_update_post( $my_post );

                            update_post_meta( get_post_thumbnail_id($my_posts[0]->ID), '_wp_attachment_image_alt', get_the_title($my_posts[0]->ID) );

                            if($blog_post['metaTitle'] != null){

                                update_post_meta( $my_posts[0]->ID, '_yoast_wpseo_title', $blog_post['metaTitle'] );
                            }

                            if($blog_post['metaDesc'] != null){

                                update_post_meta( $my_posts[0]->ID, '_yoast_wpseo_metadesc', $blog_post['metaDesc'] );
                            }


                        }else{

                            if($current_date == $post_sync_date || $current_date > $post_sync_date)
                         {

                            write_log('--------------'.$the_slug.'------------');
                            write_log('in else condition - Post not exists - Insert');

                            $args = array(
                                'post_name'        => $the_slug,                                
                                'post_type'   => 'post', 
                                'post_status' => 'draft',                              
                                'numberposts' => 1
                                );


                                write_log( $args);

                                $check_posts = get_posts($args);

                                write_log('check post');
                            
                                write_log( $check_posts);

                         if(empty($check_posts)){
    
                                 write_log( 'insert posst');

                             // Gather post data.
                             $site_post = array(
                                'post_title'    => $blog_post['name'],
                                'post_name'     => $blog_post['url'],
                                'post_content'  => $blog_post['text'],
                                'post_excerpt'  => wp_trim_words( $blog_post['text'], 50 ),
                                'post_type'     => 'post',
                                'post_category' => array( $blogid ),
                                'post_status'   => 'publish',
                                'post_author'  => 1,
                                'post_date'  => $postdate,
                                'comment_status' => 'closed',   // if you prefer
                                'ping_status' => 'closed'
                                
                            );
                            
                            // Insert the post into the database.
                         $blog_id = wp_insert_post( $site_post );

                         $tags = array($blog_post['tags']); // Array of Tags to add

                         wp_set_post_tags( $blog_id, $tags); // Set tags to Post                       
                         

                         if($blog_post['metaTitle'] != null){

                            update_post_meta( $blog_id, '_yoast_wpseo_title', $blog_post['metaTitle'] );
                        }

                        if($blog_post['metaDesc'] != null){

                            update_post_meta( $blog_id, '_yoast_wpseo_metadesc', $blog_post['metaDesc'] );
                        }

                         
                                // Add Featured Image to Post
                                    $image_url        = 'http://'.$blog_post['images'][0]['url'];
                                    $extension        = pathinfo($image_url);
                                    $image_name       = $blog_post['url'].'.'.$extension['extension'];
                                    $upload_dir       = wp_upload_dir(); // Set upload folder
                                    $image_data       = file_get_contents($image_url); // Get image data
                                    $unique_file_name = wp_unique_filename( $upload_dir['path'], $image_name ); // Generate unique name
                                    $filename         = basename( $unique_file_name ); // Create image file name

                                    // Check folder permission and define file location
                                    if( wp_mkdir_p( $upload_dir['path'] ) ) {
                                        $file = $upload_dir['path'] . '/' . $filename;
                                    } else {
                                        $file = $upload_dir['basedir'] . '/' . $filename;
                                    }

                                    // Create the image  file on the server
                                    file_put_contents( $file, $image_data );

                                $filename = $file; // Full path

                                // The ID of the post this attachment is for.
                                $parent_post_id = $blog_id;

                                // Check the type of file. We'll use this as the 'post_mime_type'.
                                $filetype = wp_check_filetype( basename( $filename ), null );

                                // Get the path to the upload directory.
                                $wp_upload_dir = wp_upload_dir();

                                // Prepare an array of post data for the attachment.
                                $attachment = array(
                                'guid'           => $wp_upload_dir['url'] . '/' . basename( $filename ),
                                'post_mime_type' => $filetype['type'],
                                'post_title'     => preg_replace( '/\.[^.]+$/', '', basename( $filename ) ),
                                'post_content'   => '',
                                'post_status'    => 'inherit'
                                );

                                // Insert the attachment.
                                $attach_id = wp_insert_attachment( $attachment, $filename, $parent_post_id );

                                // Make sure that this file is included, as wp_generate_attachment_metadata() depends on it.
                                require_once( ABSPATH . 'wp-admin/includes/image.php' );

                                // Generate the metadata for the attachment, and update the database record.
                                $attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
                                wp_update_attachment_metadata( $attach_id, $attach_data );

                                
                                // And finally assign featured image to post
                                set_post_thumbnail( $blog_id, $attach_id );
                            }
                        else{

                            $my_post = array(
                                'ID'           => $check_posts[0]->ID,                                
                                'post_status' => 'publish'
                               
                            );
                          
                          // Update the post into the database
                            wp_update_post( $my_post );

                        }
                            }
                        }

    }
}
    
   
function callSaleAPI($apiObj,$result){
    $inputs = array();
    $headers = array('authorization'=>"bearer ".$result['access_token']);
    $promos = $apiObj->call(PROMOSAPI.get_option('SITE_CODE'),"GET",$inputs,$headers);       
    
    $salespriority = json_decode(get_option('salespriority'));
    $salespriority = (array)$salespriority;
    $high_priprity = min($salespriority);

    $saleslider = array();
    $saleData = array();
    $salepriority = array();
    $slide_brands = '';
    $e = 0;
    
    if(isset($promos['success']) && $promos['result'] =='' ){

        update_option('salesliderinformation','');
        update_option('salebannerdata','');  
    }
    if(isset($promos['success']) && isset($promos['result'])  ){
        
        update_option('salesliderinformation','');
        update_option('salebannerdata','');  
        
        if(count($promos['result'])>0){
          
            $promos_json = json_encode($promos['result']);
            get_option('promos_json') || get_option('promos_json') == null?update_option('promos_json',$promos_json):add_option('promos_json',$promos_json);
            $promos = json_decode(get_option('promos_json'));
            
              $k =0;

              $y=0;

            foreach($promos as $promo){


                $salepriority[$promo->promoCode] =  $promo->priority ; 

                $saleData[$y]['sale_name'] = $promo->name;
                $saleData[$y]['promoCode'] =$promo->promoCode;
                $saleData[$y]['promoType'] =$promo->promoType;
                $saleData[$y]['formType'] =$promo->formType;                    
                $saleData[$y]['name'] = $promo->name; 
                $saleData[$y]['startDate'] =$promo->startDate; 
                $saleData[$y]['endDate'] =$promo->endDate;   
                $saleData[$y]['isRugShop'] =$promo->rugShop;
                $saleData[$y]['getCoupon'] =$promo->getCoupon;
                $saleData[$y]['brandList'] =$promo->brandList;
                

                foreach($promo->widgets as $widget){
                    
                 
                if($widget->type == "slide" && in_array( get_option('SITE_CODE'),$widget->clientCodes)  ){
                   
                    $saleslider[$k]['promoCode'] =$promo->promoCode;
                    $saleslider[$k]['name']     =$promo->name; 
                    $saleslider[$k]['priority'] = $promo->priority ; 
                    $saleslider[$k]['promoType'] =$promo->promoType;
                    $saleslider[$k]['formType'] =$promo->formType;                       
                    $saleslider[$k]['startDate'] =$promo->startDate; 
                    $saleslider[$k]['endDate'] =$promo->endDate;
                    $saleslider[$k]['slide_link'] =$widget->link;
                    $saleslider[$k]['isRugShop'] =$promo->rugShop;
                    $saleslider[$k]['getCoupon'] =$promo->getCoupon;
                    $saleslider[$k]['brandList'] =$promo->brandList;
 
                    if($promo->brandList !=''){
                        foreach($promo->brandList as $promobrand){

                        if($promo->priority ==  $high_priprity){

                            $slide_brands .= sanitize_title($promobrand).',';
                            
                           }                            

                        }
                    
                    }

                    
                  
                  $Slidearray = json_decode(json_encode($widget->images), True);
                   
                    foreach($widget->images as $slide){                         

                        if($slide->type == "graphic"){
                        //  $slider['slider_coupan_img'] =  $slide->url;
                        $saleslider[$k]['slider']['slider_coupan_img'] ="https://".$slide->url;
                        $saleslider[$k]['slider']['link'] =$widget->link;
                        $saleslider[$k]['slider']['order'] =$widget->order;
                        }							
                        
                        if($slide->type == "background"){
                        //  $slider['slider_bg_img'] =  $slide->url;
                        $saleslider[$k]['slider']['slider_bg_img'] ="https://".$slide->url;
                        $saleslider[$k]['background_image'] ="https://".$slide->url;
                        
                        }
                    }
                    $k++;
                    
                }                   

                }

                
                $y++;
            }
            
             $slider_json = json_encode($saleslider);
            update_option('salesliderinformation',$slider_json);
            $slide_brands = rtrim($slide_brands,",");
            update_option('salesbrand',$slide_brands);
            update_option('salespriority',json_encode($salepriority));
            
            $sliderdata_json = json_encode($saleData);
            update_option('saleconfiginformation',$salepriority);           

            
           $s = 0;
            foreach($promos as $promo){


                $seleinformation[$promo->promoCode]['sale_name'] = $promo->name;
                $seleinformation[$promo->promoCode]['promoCode'] =$promo->promoCode;
                $seleinformation[$promo->promoCode]['name'] =$promo->name; 
                $seleinformation[$promo->promoCode]['priority'] =$promo->priority; 
                $seleinformation[$promo->promoCode]['promoType'] =$promo->promoType;
                $seleinformation[$promo->promoCode]['formType'] =$promo->formType;     
                $seleinformation[$promo->promoCode]['formName'] =$promo->formName;  
                $seleinformation[$promo->promoCode]['brandList'] =$promo->brandList;                 
                $seleinformation[$promo->promoCode]['startDate'] =$promo->startDate; 
                $seleinformation[$promo->promoCode]['endDate'] =$promo->endDate;
                $seleinformation[$promo->promoCode]['isRugShop'] =$promo->rugShop;
                $seleinformation[$promo->promoCode]['getCoupon'] =$promo->getCoupon;
                $seleinformation[$promo->promoCode]['brandList'] =$promo->brandList;

                foreach($promo->widgets as $widget){
                    
                    
               
                    if ($widget->type == "message") {
                        $i =0;
                        $seleinformation[$promo->promoCode]['message_link'] =$widget->link;
                        foreach($widget->texts as $text ){
                            if($text->type =="copy"){
                                $seleinformation[$promo->promoCode]['message'] =$text->text;
                            //   var_dump($seleinformation);
                            }
                            
                            
                        }
                        $i++;
                    }
                    if ($widget->type == "navigation") {
                        $i =0;
                        $seleinformation[$promo->promoCode]['navigation_link'] =$widget->link;
                        foreach($widget->images as $slide){

                            if($slide->type == "graphic"){
                            //  $slider['slider_coupan_img'] =  $slide->url;
                            $seleinformation[$promo->promoCode]['navigation_img'] ="https://".$slide->url;
                            }
                        }
                        foreach($widget->texts as $text ){
                            if($text->type =="copy"){
                                $seleinformation[$promo->promoCode]['navigation_text'] =$text->text;
                            //   var_dump($seleinformation);
                            }
                            
                            
                            
                        }
                        $i++;
                    }
                    
                    if ($widget->type == "banner") {
                        $i =0;
                        $seleinformation[$promo->promoCode]['banner_link'] =$widget->link;
                        foreach($widget->images as $slide){
                            
                            if($widget->name == "Category Banner, Category Page "){

                                if($slide->type == "mobile"){
                                    //  $slider['slider_coupan_img'] =  $slide->url;
                                    $seleinformation[$promo->promoCode]['category_banner_img_mobile'] ="https://".$slide->url;
                                    }
                                    if($slide->type == "desktop"){
                                    //  $slider['slider_bg_img'] =  $slide->url;
                                    $seleinformation[$promo->promoCode]['category_banner_img_deskop'] ="https://".$slide->url;
                                    
                                    }    


                            }else{
                            
                                    if($slide->type == "mobile"){
                                    //  $slider['slider_coupan_img'] =  $slide->url;
                                    $seleinformation[$promo->promoCode]['banner_img_mobile'] ="https://".$slide->url;
                                    }
                                    if($slide->type == "desktop"){
                                    //  $slider['slider_bg_img'] =  $slide->url;
                                    $seleinformation[$promo->promoCode]['banner_img_deskop'] ="https://".$slide->url;
                                    
                                    }
                         }
                        }
                        $i++;
                    }
                    if ($widget->type == "banner") {
                             
                        
                        
                        $saleBanner[$e]['name'] = $widget->name;
                        $saleBanner[$e]['type'] = $widget->type;
                        $saleBanner[$e]['link'] =  $widget->link;
                        $saleBanner[$e]['priority'] =  $promo->priority;
                        $saleBanner[$e]['promoCode'] =  $promo->promoCode;
                        $saleBanner[$e]['rugShop'] =  $promo->rugShop;
                        $saleBanner[$e]['name'] =  $promo->name;

                        foreach($widget->images as $banner){  

                                    $saleBanner[$e]['images'][$banner->type] = "https://".$banner->url;                                        
                                    $saleBanner[$e]['size'] = $banner->size; 
                                    
                                    if($banner->size == 'fullwidth'){

                                       
                                        $seleinformation[$promo->promoCode]['banner']['fullwidth']['size'] = $banner->size;
                                        $seleinformation[$promo->promoCode]['banner']['fullwidth']['link'] = $widget->link;
                                        if($banner->type == 'desktop'){
                                            $seleinformation[$promo->promoCode]['banner']['fullwidth']['desktop_url'] = $banner->url;   
                                        }
                                        if($banner->type == 'mobile'){
                                            $seleinformation[$promo->promoCode]['banner']['fullwidth']['mobile_url'] = $banner->url;   
                                        }
                                        
                                    }

                                    if($banner->size == 'fixedwidth'){

                                       
                                        $seleinformation[$promo->promoCode]['banner']['fixedwidth']['size'] = $banner->size;
                                        $seleinformation[$promo->promoCode]['banner']['fixedwidth']['link'] = $widget->link;
                                        if($banner->type == 'desktop'){
                                            $seleinformation[$promo->promoCode]['banner']['fixedwidth']['desktop_url'] = $banner->url;   
                                        }
                                        if($banner->type == 'mobile'){
                                            $seleinformation[$promo->promoCode]['banner']['fixedwidth']['mobile_url'] = $banner->url;   
                                        }
                                        
                                    }
                                   
                        }
                        $e++;
                    }
                    if($widget->type == "slide" ){
                        $i =0;
                        $seleinformation[$promo->promoCode]['slide_link'] =$widget->link;
                        foreach($widget->images as $slide){

                            if($slide->type == "graphic"){
                            //  $slider['slider_coupan_img'] =  $slide->url;
                            $seleinformation[$promo->promoCode]['slider'][$i]['slider_coupan_img'] ="https://".$slide->url;
                            $seleinformation[$promo->promoCode]['slider'][$i]['link'] =$widget->link;
                            }
                            if($slide->type == "background"){
                            //  $slider['slider_bg_img'] =  $slide->url;
                            $seleinformation[$promo->promoCode]['slider'][$i]['slider_bg_img'] ="https://".$slide->url;
                            $seleinformation[$promo->promoCode]['background_image'] ="https://".$slide->url;
                            
                            }
                        }
                        $i++;
                        
                    }
                    if($widget->type == "landing" ){
                        
                        $seleinformation[$promo->promoCode]['landing_link'] = $widget->link;
                        foreach($widget->images as $slide){
                            
                            if($slide->type == "graphic"){
                                $seleinformation[$promo->promoCode]['slider_coupan_img'] = "https://".$slide->url;
                                $seleinformation[$promo->promoCode]['image_onform'] ="https://".$slide->url;
                                
                            }

                            if($slide->type == "background"){
                                //  $slider['slider_bg_img'] =  $slide->url;
                                //$seleinformation['slider'][$i]['slider_bg_img'] ="https://".$slide->url;
                                $seleinformation[$promo->promoCode]['background_image_landing'] ="https://".$slide->url;
                                
                            }
                            if($slide->type == "mobile"){
                                //  $slider['slider_bg_img'] =  $slide->url;
                                //$seleinformation['slider'][$i]['slider_bg_img'] ="https://".$slide->url;
                                $seleinformation[$promo->promoCode]['image_onform_mobile'] ="https://".$slide->url;
                                
                            }
                        
                        }
                        
                        foreach($widget->texts as $text ){
                           
                            if(strcasecmp($text->type,"copy") ==0){
                            //if($text->type =="Copy"){
                               // echo "HERER";
                             //echo ($text->type);
                              $seleinformation[$promo->promoCode]['content'] =$text->text;
                            //exit;

                           
                            }
                           // echo "NO";exit;
                           if(strcasecmp($text->type,"heading") ==0){
                            //if($text->type =="heading"){
                                $seleinformation[$promo->promoCode]['saveupto_doller'] =$text->text;
                            
                            }
                            if(strcasecmp($text->type,"subheading") == 0){
                                
                                $seleinformation[$promo->promoCode]['subheading'] =$text->text;
                            
                            }
                            
                            
                        }
                        
                    }

                    
                
                }//Widget
                
                $s++;
            }
            //$seleinformation = json_decode(get_option('saleinformation'));

           // echo $seleinformation;
            //exit;
            update_option('saleinformation',json_encode($seleinformation));
            update_option('salebannerdata',json_encode($saleBanner));
        }
        else{
            $_SESSION['error'] = "Promos Information API";
            $_SESSION["error_desc"] ="We didn't get sale information from API OR Sale is not assigned to this site";
            get_option('promos_json')?update_option('promos_json',""):"";
            get_option('saleinformation')?update_option('saleinformation',""):"";
            update_option('salesliderinformation','');
            update_option('salebannerdata',''); 
        }
    }
    else{
        
        $msg =$promos['message'];                
        $_SESSION['error'] = "Promos Information API";
        $_SESSION["error_desc"] =$msg;
        get_option('promos_json')?update_option('promos_json',""):"";
        get_option('saleinformation')?update_option('saleinformation',""):"";
        update_option('salesliderinformation','');
    }
    
 }
    

     function colorwall_page_template( $page_template )
{
    if ( is_page( 'choose-your-style' ) ) {
        $page_template = dirname( __FILE__ ) . 'product-listing-templates/choose_your_style.php';
    }
    return $page_template;
}


/**  Plugin updater checker integration **/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
    //'https://bitbucket.org/user-name/repo-name',
    'https://bitbucket.org/mobilemarketingllc/grandchild-plugin',
	__FILE__,
	'grand-child'
);

$myUpdateChecker->setAuthentication(array(
	'consumer_key' => 'Cn64bdU6RGrTTYpq4c',
	'consumer_secret' => 'fBxTEShRKubNn4WxrDDBymH4e4rGfqX6',
));



//Optional: Set the branch that contains the stable release.
if(ENV == 'dev' || ENV == 'staging'){
    //var_dump($myUpdateChecker->getBranch());
   // echo '<pre>';
    $myUpdateChecker->setBranch('dev');
   // exit;
    
}
else if(ENV == 'prod'){
    $myUpdateChecker->setBranch('master');
}




function documentation_func(){
    ?>
        <div class="wrap" id="grandchild-backend">
            <h2>ShortCodes - Documentation </h2>
            <div class="description"></div>
                <table class="widefat striped" border="1" style="border-collapse:collapse;">
                    <thead>
                        <tr>
                            <th width="30%"><strong>Shortcode</strong></th>
                            <th width="35%"><strong>Example</strong></th>
                            <th width="30%"><strong>Description</strong></th>
                        </tr>
                        <tr class="saleapi header-tr">
                            <th colspan="3">Sale information</th>
                        </tr>
                        <tr class="saleapi">
                            <td> [coupon "homepage_banner"]</td>
                            <td> [coupon "homepage_banner"]</td>
                            <td>Use this shortcode if there is no slider on homepage.</td>
                        </tr>
                        <tr class="saleapi">
                            <td> [coupon "banner"]</td>
                            <td> [coupon "banner"]</td>
                            <td>Use this shortcode for display only banner.</td>
                        </tr>
                        <tr class="saleapi">
                            <td> [coupon "navigation" "text"]</td>
                            <td> [coupon "navigation" "text"]</td>
                            <td>Top for navigation text (Save up to $1000)</td>
                        </tr>
                        <tr class="saleapi">
                            <td> [coupon "navigation" "image"]</td>
                            <td> [coupon "navigation" "image"]</td>
                            <td>In navigation if there is banner images of sale.</td>
                        </tr>
                        <tr class="saleapi">
                            <td> [coupon "message"]</td>
                            <td> [coupon "message"]</td>
                            <td>Sale Message Block ( Save $1,000 OFF Your Next Flooring Purchase)</td>
                        </tr>
                        <tr class="saleapi">
                            <td>[coupon "salebanner" "full"]</td>
                            <td>[coupon "salebanner" "full"] OR [coupon "salebanner"]</td>
                            <td>Display the promo banner images  having Full width and height (2800 by 200).</td>
                        </tr>
                        <tr class="saleapi">
                            <td>[coupon "salebanner" "fixed"]</td>
                            <td>[coupon "salebanner" "fixed"]</td>
                            <td>Display the promo banner images  having Fixed width and height (1800 by 180).</td>
                        </tr>
                        <tr class="saleapi">
                            <td>[coupon "heading"]</td>
                            <td>[coupon "heading"]</td>
                            <td>Display the heading (Save upto $1000 text ).</td>
                        </tr>
                        <tr class="saleapi">
                            <td>[coupon "content"]</td>
                            <td>[coupon "content"]</td>
                            <td>Display the text below the form on flooring coupon page.</td>
                        </tr>
                        <tr class="saleapi">
                            <td>[coupon "image_onform" "background_img"]</td>
                            <td>[coupon "image_onform" "background_img"]</td>
                            <td>Display the image beside the form on flooring coupon page.(background_img is option if we requied backgrond image)</td>
                        </tr>
                        <tr class="saleapi">
                            <td>[coupon "print_coupon"]</td>
                            <td>[coupon "print_coupon"]</td>
                            <td>Display the print image button with print coupon button</td>
                        </tr>
                        <tr class="saleapi">
                            <td>[coupon "popup_img"]</td>
                            <td>[coupon "popup_img"]</td>
                            <td>Display the popup image with coupon landing page link</td>
                        </tr>
                        <tr class="saleapi header-tr">
                            <th colspan="3">Area Rug Information</th>
                        </tr>
                        <tr class="arearug">
                            <td>[areagallery]</td>
                            <td>[areagallery]</td>
                            <td>Display the Area Rug Image Gallery.</td>
                        </tr>
                        <tr class="arearug">
                            <td><div class="copy">[shoparearug]</div></td>
                            <td>[shoparearug]</td>
                            <td>Display Area rugs six categories with area rug site link.</td>
                        </tr>
                        <tr class="arearug">
                            <td>[area_rug_trading_products]</td>
                            <td>[area_rug_trading_products]</td>
                            <td>Display Area rugs trading products with area rug site link.</td>
                        </tr>
                        <tr class="arearug">
                            <td>[rugshop_button "title"]</td>
                            <td>[rugshop_button "SHOP RUGS"]</td>
                            <td>Display button with area rugshop affiliate site link.</td>
                        </tr>
                        <tr class="arearug">
                            <td>[rugshop_link "title"]</td>
                            <td>[rugshop_link "SHOP RUGS"]</td>
                            <td>Display anchor tag with area rugshop affiliate site link.</td>
                        </tr>
                        <tr class="arearug">
                            <td>[affiliate_code]</td>
                            <td>https://rugs.shop/en_us/oriental-weavers-henderson-625w-grey-625w9/?store=[affiliate_code]</td>
                            <td>Return area rugshop affiliate code only.</td>
                        </tr>
                        <tr class="saleapi header-tr">
                            <th colspan="3">Store Information</th>
                        </tr>
                        <tr class="address">
                            <td>[storelocation_address "dir" "location name"] </td>
                            <td>[storelocation_address "dir" "HAMERNICK'S INTERIOR SOLUTIONS" ]</td>
                            <td>Display button with "Get Direction" text link to the store address</td>
                        </tr>
                        <tr class="address">
                            <td>[storelocation_address "map" "location name"] </td>
                            <td>[storelocation_address "map" "HAMERNICK'S INTERIOR SOLUTIONS" ]</td>
                            <td>Display map of the store address</td>
                        </tr>
                        <tr class="address">
                            <td>[storelocation_address "loc" "location name"] </td>
                            <td>[storelocation_address "loc" "HAMERNICK'S INTERIOR SOLUTIONS" ]</td>
                            <td>Display the store address with having link to the google map.</td>
                        </tr>
                        <tr class="address">
                            <td>[storelocation_address "loc" "location name" "nolink"] </td>
                            <td>[storelocation_address "loc" "HAMERNICK'S INTERIOR SOLUTIONS" "nolink"]</td>
                            <td>Display the store address with having without link to the google map.</td>
                        </tr>
                        <tr class="address">
                            <td>[storelocation_address "forwardingphone" "location name"] </td>
                            <td>[storelocation_address "forwardingphone" "HAMERNICK'S INTERIOR SOLUTIONS"]</td>
                            <td>Display the forwading number of the store with link.</td>
                        </tr>
                        <tr class="address">
                            <td>[storelocation_address "forwardingphone" "location name" "nolink"] </td>
                            <td>[storelocation_address "forwardingphone" "HAMERNICK'S INTERIOR SOLUTIONS" "nolink"]</td>
                            <td>Display the forwading number of the store without link.</td>
                        </tr>
                        <tr class="address">
                            <td>[storelocation_address "ohrs" "location name"]</td>
                            <td>[storelocation_address "ohrs" "HAMERNICK'S INTERIOR SOLUTIONS"]</td>
                            <td>Display the store location opening and closing hours.</td>
                        </tr>
                        <tr class="saleapi header-tr">
                            <th colspan="3">Retailer Information</th>
                        </tr>
                        <tr class="retailer">
                            <td>[Retailer "city"]</td>
                            <td>[Retailer "city"]</td>
                            <td>Display the social icons.</td>
                        </tr>
                        <tr class="retailer">
                            <td>[Retailer "state"]</td>
                            <td>[Retailer "state"]</td>
                            <td>Display the state of first store address which are added.</td>
                        </tr>
                        <tr class="retailer">
                            <td>[Retailer "zipcode"]</td>
                            <td>[Retailer "zipcode"]</td>
                            <td>Display the zipcode of first store address which are added.</td>
                        </tr>
                        <tr class="retailer">
                            <td>[Retailer "legalname"]</td>
                            <td>[Retailer "legalname"]</td>
                            <td>Display the legalname of site.</td>
                        </tr>
                        <tr class="retailer">
                            <td>[Retailer "address"]</td>
                            <td>[Retailer "address"]</td>
                            <td>Display the address line of site.</td>
                        </tr>
                        <tr class="retailer">
                            <td>[Retailer "phone" "nolink"]</td>
                            <td>[Retailer "phone"]</td>
                            <td>Display the phone without of site.</td>
                        </tr>
                        <tr class="retailer">
                            <td>[Retailer "phone"]</td>
                            <td>[Retailer "phone"]</td>
                            <td>Display the phone of with link.</td>
                        </tr>
                        <tr class="retailer">
                            <td>[Retailer "forwarding_phone"]</td>
                            <td>[Retailer "forwarding_phone"]</td>
                            <td>Display the forwarding phone with link of tel.</td>
                        </tr>
                        <tr class="retailer">
                            <td>[Retailer "forwarding_phone" "nolink"]</td>
                            <td>[Retailer "forwarding_phone" "nolink"]</td>
                            <td>Display the Forwarding phone without the link .</td>
                        </tr>
                        <tr class="retailer">
                            <td>[Retailer "companyname"]</td>
                            <td>[Retailer "companyname"]</td>
                            <td>Display the Company Name of site.</td>
                        </tr>
                        <tr class="retailer">
                            <td>[Retailer "site_url"]</td>
                            <td>[Retailer "site_url"]</td>
                            <td>Display the Site URL of site.</td>
                        </tr>
                        <tr class="saleapi header-tr">
                            <th colspan="3">Site Information</th>
                        </tr>
                        <tr class="social">
                            <td>[copyrights]</td>
                            <td>[copyrights]</td>
                            <td>Display the copyrights text use in footer.</td>
                        </tr>
                        <tr class="social">
                            <td>[getSocailIcons]</td>
                            <td>[getSocailIcons]</td>
                            <td>Display the social icons.</td>
                        </tr>
                        <tr class="social">
                            <td>[chat_meter]</td>
                            <td>[chat_meter]</td>
                            <td>Display Reviews of location</td>
                        </tr>
                        <tr class="social">
                            <td>[wells_fargo_finance "button text"]</td>
                            <td>[wells_fargo_finance "Apply for financing"]</td>
                            <td>Display wells fargo finance link button</td>
                        </tr>
                        <tr class="social">
                            <td>[wells_fargo_finance_link]</td>
                            <td>[wells_fargo_finance_link]</td>
                            <td>Returns wells fargo finance link</td>
                        </tr>
                        <tr class="social">
                            <td>[synchrony_finance "button text"]</td>
                            <td>[synchrony_finance "Apply for financing"]</td>
                            <td>Display synchrony finance link button</td>
                        </tr>
                        <tr class="social">
                            <td>[synchrony_finance_link]</td>
                            <td>[synchrony_finance_link]</td>
                            <td>Returns synchrony finance link</td>
                        </tr>
                        <tr class="social">
                            <td>[Link "site respective url"]</td>
                            <td>[Link "/about/location/"]</td>
                            <td>Returns provided link if page/post exists in site , if not then return home page url (/)</td>
                        </tr>
                        
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    <?php
}

//Create Form for client code and site id:
    function Calling_API_form($site,$clientcode) {
        if(isset($_SESSION['error'])){
     ?>
        <div class="notice notice-info error-info is-dismissible woo-info">
            <div class="info-image">
                <p> <img src='<?php echo plugins_url("/img/info-alt.png", __FILE__)?>' width="48px"/></p>
            </div>
            <div class="info-descriptions">
                <div class="info-descriptions-title">
                        <h3><strong><?php echo ucfirst($_SESSION["error"]);?></strong></h3>
                </div>
                <p><?php echo $_SESSION["error_desc"];?></p>
                <p><b>Please Contact Plugin Development Team for further details</b></p>
            </div>
        </div>
        <?php
        }

        $is_own_templates = get_option('is_own_templates');      
          
        $sh_get_finance = get_option('sh_get_finance');
        
        $hide_getcoupon_button = get_option('hide_getcoupon_button'); 

        $form = '
        <div id="wpcontent1" class="client_info_wrap">
            <form name="pluginname" action="'.$_SERVER['REQUEST_URI'].'" method="POST">
                <table class="form-table">
                    <tr>
                        <th colspan="2"><h4>Enter Retailer Info</h4></th>
                    </tr>
                    '.( isset( $msg) ? '
                    <tr>
                        <td colspan="2">
                        <span class="alert alert-danger"><em class="dashicons dashicons-warning"></em> '.$msg.'</span>
                            '.( !isset( $_POST['siteid'] ) ? '<span class="alert alert-danger"><em class="dashicons dashicons-warning"></em> Site ID cannot be blank</span>' : null ).'
                            '.( !isset( $_POST['clientcode'] ) ? '<span class="alert alert-danger"><em class="dashicons dashicons-warning"></em> Client Code cannot be blank</span>' : null ).'
                        </td>
                    </tr>
                    ' : null ).'
                    <tr>
                        <td width="150px">
                            <label for="siteid">Select environment <strong>*</strong></label>
                        </td>';
                        $cde = (get_option('CDE_ENV')) && get_option('CDE_ENV')!="" ?get_option('CDE_ENV'):""; 
                        $lasttimesync = get_option('CDE_LAST_SYNC_TIME')?date("F j, Y, g:i a", get_option('CDE_LAST_SYNC_TIME')):"Not yet sync";
                        if($cde != ""){
                            $form .='<td class="form-group">
                            <select name="instance-select">
                                <option value="prod" '.(  $cde=="prod" ? 'selected=selected' : null ).'>Production</option>
                                <option value="staging" '.(  $cde=="staging" ? 'selected=selected' : null ).'>Staging</option>
                                <option value="dev"  '.(  $cde=="dev" ? 'selected=selected' : null ).'>Development</option>
                            </select>
                        </td></tr>';
                        }
                        else{
                            $form .= '<td class="form-group">
                            <select name="instance-select" required>
                                <option value="">Choose Environment</option>
                                <option value="prod">Production</option>
                                <option value="staging">Staging</option>
                                <option value="dev">Development</option>
                            </select>
                        </td></tr>';
                        }
                        
                    
         $form .= '<tr>
                        <td width="150px">
                            <label for="siteid">Client Code <strong>*</strong></label>
                        </td>
                        <td class="form-group">
                            <input type="text" name="siteid" value="' . ( (get_option('SITE_CODE') ) ? get_option('SITE_CODE') : null ) . '" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="clientcode">Client Username <strong>*</strong></label>
                        </td>
                        <td class="form-group">
                            <input type="text" name="clientcode" value="' . ( get_option('CLIENT_CODE') ? get_option('CLIENT_CODE') : null ) . '" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="clientsecret">Client Secret <strong>*</strong></label>
                        </td>
                        <td class="form-group">
                            <input type="text" name="clientsecret" value="' . ( get_option('CLIENTSECRET') ? get_option('CLIENTSECRET') : null ) . '" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="form-group">
                            <button id="syncdata" type="submit" class="button button-primary" >Sync</button>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:right">Last sync time : '.$lasttimesync.'</td>
                    </tr>
                    
                </table>    
            </form>

            <form name="pluginname" action="'.$_SERVER['REQUEST_URI'].'" method="POST">
            <table class="form-table">
                <tr>
                    <th colspan="2"><h4>PDP Layout Settings</h4></th>
                </tr>
                <tr>
                    <td width="150px">
                        <label for="clientsecret">Product Detail Layout <strong>*</strong></label>
                    </td>
                    <td class="form-group">
                        <select name="layoutopotion" >
                            <option value="0" '.(  get_option('layoutopotion')==0 ? 'selected=selected' : null ).'>Default</option>
                            <option value="1" '.(  get_option('layoutopotion')==1 ? 'selected=selected' : null ).'>1</option>
                            <option value="2" '.(  get_option('layoutopotion')==2 ? 'selected=selected' : null ).'>2</option>
                            <option value="3" '.(  get_option('layoutopotion')==3 ? 'selected=selected' : null ).'>3</option>
                            <option value="4" '.(  get_option('layoutopotion')==4 ? 'selected=selected' : null ).'>4</option>
                            <option value="5" '.(  get_option('layoutopotion')==5 ? 'selected=selected' : null ).'>5</option>
                            <option value="6" '.(  get_option('layoutopotion')==6 ? 'selected=selected' : null ).'>6</option>
                            <option value="ccc" '.(  get_option('layoutopotion')=='ccc' ? 'selected=selected' : null ).'>CCC</option>
                        </select>
                    </td>
                </tr>
                <tr>
                <td width="150px">
                    <label for="clientsecret">Use your own theme templates <strong>*</strong></label>
                </td>
                <td class="form-group">                   
                    <select name="is_own_templates" >
                    <option value="0" '.(  get_option('is_own_templates')==0 ? 'selected=selected' : null ).'>No</option>
                    <option value="1" '.(  get_option('is_own_templates')==1 ? 'selected=selected' : null ).'>Yes</option>                    
                </select>
                </td>
            </tr>
            <tr>
                <td width="150px">
                    <label for="clientsecret">Show "Get Financing Button" on PLP pages? <strong>*</strong></label>
                </td>
                <td class="form-group">                    
                    <select name="sh_get_finance" >
                    <option value="0" '.(  get_option('sh_get_finance')==0 ? 'selected=selected' : null ).'>No</option>
                    <option value="1" '.(  get_option('sh_get_finance')==1 ? 'selected=selected' : null ).'>Yes</option>                    
                </select>
                </td>
            </tr>
            <tr>
                <td width="150px">
                    <label for="clientsecret">Hide "Get Financing Button" on PDP pages?<strong>*</strong></label>
                </td>
                <td class="form-group">                    
                    <select name="pdp_get_finance" >
                    <option value="0" '.(  get_option('pdp_get_finance')==0 ? 'selected=selected' : null ).'>No</option>
                    <option value="1" '.(  get_option('pdp_get_finance')==1 ? 'selected=selected' : null ).'>Yes</option>                    
                </select>
                </td>
            </tr>

            <tr>
            <td width="150px">
                <label for="clientsecret">Do you want to change Get financing text with another? <strong>*</strong></label>
            </td>
            <td class="form-group">                   
                <select name="getfinancereplace" id="getfinancereplace">
                <option value="0" '.(  get_option('getfinancereplace')==0 ? 'selected=selected' : null ).'>No</option>
                <option value="1" '.(  get_option('getfinancereplace')==1 ? 'selected=selected' : null ).'>Yes</option>                    
            </select>
            </td>
        </tr>
        
        <tr class="row_fin" '.(  get_option('getfinancereplace')==1 ? null : 'style="display:none"' ).'>
            <td width="150px">
                <label for="clientsecret">Button Text for replace(Get financing) on PLP and PDP pages? <strong>*</strong></label>
            </td>
            <td class="form-group">                    
                <input type="text" name="getfinancetext" value="' . ( get_option('getfinancetext') ? get_option('getfinancetext') : null ) . '" />
            </td>
        </tr>

        <tr class="row_fin" '.(  get_option('getfinancereplace')==1 ? null : 'style="display:none"' ).'>
            <td width="150px">
                <label for="clientsecret">Button url for replace(Get Financing) on PLP and PDP pages? <strong>*</strong></label>
            </td>
            <td class="form-group">                    
                <input type="text" name="getfinancereplaceurl" value="' . ( get_option('getfinancereplaceurl') ? get_option('getfinancereplaceurl') : null ) . '" />
            </td>
        </tr>

              <tr>
                    <td></td>
                    <td class="form-group">
                        <button id="syncdata" type="submit" class="button button-primary" >Save</button>
                    </td>
                </tr>
            </table>    
        </form>

        <form name="pluginname" action="'.$_SERVER['REQUEST_URI'].'" method="POST">
        <table class="form-table">
            <tr>
                <th colspan="2"><h4>GET COUPON Button Setting</h4></th>
            </tr>
            <tr>
                <td width="150px">
                    <label for="clientsecret">Show Get Coupon button on PLP and PDP pages <strong>*</strong></label>
                </td>
                <td class="form-group">
                    <select name="getcouponbtn">
                        <option value="1" '.(  get_option('getcouponbtn')==1 ? 'selected=selected' : null ).'>Yes</option>
                        <option value="0" '.(  get_option('getcouponbtn')==0 ? 'selected=selected' : null ).'>No</option>                            
                    </select>
                </td>
            </tr>
            
        <tr>
            <td width="150px">
                <label for="clientsecret">Do you want to replace Get Coupon with another? <strong>*</strong></label>
            </td>
            <td class="form-group">                   
                <select name="getcouponreplace" id="getcouponreplace">
                <option value="0" '.(  get_option('getcouponreplace')==0 ? 'selected=selected' : null ).'>No</option>
                <option value="1" '.(  get_option('getcouponreplace')==1 ? 'selected=selected' : null ).'>Yes</option>                    
            </select>
            </td>
        </tr>
        
        <tr class="row_dim" '.(  get_option('getcouponreplace')==1 ? null : 'style="display:none"' ).'>
            <td width="150px">
                <label for="clientsecret">Button Text for replace(Get Coupon) on PLP and PDP pages? <strong>*</strong></label>
            </td>
            <td class="form-group">                    
                <input type="text" name="getcouponreplacetext" value="' . ( get_option('getcouponreplacetext') ? get_option('getcouponreplacetext') : null ) . '" />
            </td>
        </tr>

        <tr class="row_dim" '.(  get_option('getcouponreplace')==1 ? null : 'style="display:none"' ).'>
            <td width="150px">
                <label for="clientsecret">Button url for replace(Get Coupon) on PLP and PDP pages? <strong>*</strong></label>
            </td>
            <td class="form-group">                    
                <input type="text" name="getcouponreplaceurl" value="' . ( get_option('getcouponreplaceurl') ? get_option('getcouponreplaceurl') : null ) . '" />
            </td>
        </tr>
          
      
            <tr>
                <td></td>
                <td class="form-group">
                    <button id="syncdata" type="submit" class="button button-primary" >Save</button>
                </td>
            </tr>
        </table>    
       
    </form>

    <form name="pluginname" action="'.$_SERVER['REQUEST_URI'].'" method="POST">
    <table class="form-table">
        <tr>
            <th colspan="2"><h4>PLP Layout Settings</h4></th>
        </tr>
        <tr>
            <td width="150px">
                <label >PLP Layout <strong>*</strong></label>
            </td>
            <td class="form-group">
                <select name="plplayout" >
                    <option value="0" '.(  get_option('plplayout') !=1 ? 'selected=selected' : null ).'>Clean Version</option>
                    <option value="1" '.(  get_option('plplayout') == 1 ? 'selected=selected' : null ).'>Card Version - Popup</option>
                </select>
            </td>
        </tr>
        <tr>
        <td width="150px">
            <label>Use product images<strong>*</strong></label>
        </td>
        <td class="form-group">                   
            <select name="plpproductimg" >
            <option value="0" '.(  get_option('plpproductimg') !=0 ? 'selected=selected' : null ).'>Swatch Images</option>
            <option value="1" '.(  get_option('plpproductimg') ==1 ? 'selected=selected' : null ).'>Gallery Images</option>                    
        </select>
        </td>
    </tr>
      <tr>
            <td></td>
            <td class="form-group">
                <button id="syncdata" type="submit" class="button button-primary" >Save</button>
            </td>
        </tr>
    </table>    
</form>


        <form name="pluginname" action="'.$_SERVER['REQUEST_URI'].'" method="POST">
            <table class="form-table">
                <tr>
                    <td colspan="2"><h4>Blog Post Sync Settings</h4></td>
                    <td class="form-group">
                        <button id="syncdata" type="submit" class="button button-primary" >Sync Blog Post</button>
                    </td>
                </tr>
               <input type="hidden" name="blogsync" value="yes">
                <tr>
                    
                    
                </tr>
            </table>    
        </form>
        
        </div>
        ';
        echo $form;
        unset($_SESSION["error"]);
        unset($_SESSION["error_desc"]);
        
        
    }

    


function getRetailerInformation(){
    return '
		<div class="fl-button-wrap fl-button-width-auto fl-button-left">
			'.get_option('retailer_details').'
                    
</div>
	';
}
add_shortcode( 'getRetailerInformation', 'getRetailerInformation' );

function my_menu_page() {
        ?>
        <?php  
        if( isset( $_GET[ 'tab' ] ) ) {  
            $active_tab = $_GET[ 'tab' ];  
        } else {
            $active_tab = 'tab_one';
        }
        ?>  
        <div class="wrap" id="grandchild-backend">
            <h2>Retailer Settings <?php echo get_option('timezone_string');?></h2>
            <div class="description"></div>

            <?php Calling_API_form('','');?>
            <?php settings_errors(); ?> 

            <?php
                //$details = json_decode(get_option('retailer_details'));
                $website_json =  json_decode(get_option('website_json'));
                $details = json_decode(get_option('social_links'));
                if($website_json || $details) { ?>
                <table class="widefat striped" border="1" style="border-collapse:collapse;">
                    <thead>
                        <tr>
                            <th width="25%"><strong>Name</strong></th>
                            <th width="75%"><strong>Values</strong></th>
                        </tr>
                    </thead>
                    <tbody>
                <?php
                   
                    // foreach($website_json as $key => $value){   
                    //     if(!is_array($value) && $key !=''){
                    //         echo "<tr><td>".ucfirst($key)."</td><td>".ucfirst($value)."</td></tr>";
                    //     }   
                        
                    // }
                    echo "<tr><th colspan='2'><strong>Social Platforms</strong></th></tr>";
                    foreach($details as $key => $value){   
                        echo "<tr><td>".ucfirst($value->platform)."</td><td>".$value->url."</td></tr>";
                    }
                    echo "<tr><th colspan='2'><strong>Site information (".ENV.")</strong></th></tr>";
                    $website =  $website_json;
                    for($i=0;$i<count($website->sites);$i++){
                        if($website->sites[$i]->instance == ENV){
                            foreach($website->sites[$i] as $key => $value){   
                                if($key)
                                echo "<tr><td>".ucfirst($key)."</td><td>".($value)."</td></tr>";
                            }
                        }
                   
                }
                    echo "<tr><th colspan='2'><strong>Locations</strong></th></tr>";
                    $website =  $website_json;
                    for($i=0;$i<count($website->locations);$i++){

                        $location_name = isset($website_json->locations[$i]->name)?$website->locations[$i]->name:"";
                        $location = '';
                        //$website->locations[$i]->address.", ".$website->locations[$i]->city.", ".$website->locations[$i]->state.", ".$website->locations[$i]->postalCode;
                        $location_address  = isset($website->locations[$i]->address)?$website->locations[$i]->address:"";
                        $location_address .= isset($website->locations[$i]->city)?" , ".$website->locations[$i]->city:"";
                        $location_address .= isset($website->locations[$i]->state)?" , ".$website->locations[$i]->state:"";
                        $location_address .= isset($website->locations[$i]->postalCode)?" , ".$website->locations[$i]->postalCode:"";
                        
                        
                
                        $location_phone = isset($website->locations[$i]->phone)?$website->locations[$i]->phone:"";
                        echo "<tr><td>Name</td><td><b>".$location_name."</b></td></tr>";
                        echo "<tr><td>Address</td><td>".$location_address."</td></tr>";
                        echo "<tr><td>Phone</td><td>".$website->locations[$i]->phone."</td></tr>";
                        echo "<tr><td>Forwarding phone</td><td>".$website->locations[$i]->forwardingPhone."</td></tr>";
                        echo "<tr><td>License Number</td><td>".$website->locations[$i]->licenseNumber."</td></tr>";
                        $weekdays = array("monday","tuesday","wednesday","thursday","friday","saturday","sunday");
                        
                        $openinghrs = "<ul style='display:block;'>";
                        for ($j = 0; $j < count($weekdays); $j++) {
                            $location .= $website->locations[$i]->monday;
                            if (isset($website->locations[$i]->{$weekdays[$j]})) {
                                $openinghrs .= '<li>'.ucfirst($weekdays[$j]).' : <span>'.$website->locations[$i]->{$weekdays[$j]}.'</span></li>';
                            }
                        }
                        $openinghrs .= "</ul>";
                        echo "<tr><td>Opening Hrs</td><td>".$openinghrs."</td></tr>";
                    }
                    $contacts = json_decode(get_option('website_json'));
                    echo "<tr><th colspan='2'><strong>Contacts</strong></th></tr>";

                    if(is_array($contacts->contacts)){
                        
                        for($j=0;$j<count($contacts->contacts);$j++){
                            foreach($contacts->contacts[$j] as $key => $value){   
                                echo "<tr><td>".ucfirst($key)."</td><td>".$value."</td></tr>";
                            }
                        }
                    }
                    
                  //  echo do_shortcode("[storelocation_address alldata]"); 
                ?>
                    </tbody>
                </table>
                <?php 
                }
                    
            ?>

            <h2 class="nav-tab-wrapper" style="display:none;" >  
                <a href="?page=my-menu-slug&tab=tab_one" class="nav-tab <?php echo $active_tab == 'tab_one' ? 'nav-tab-active' : ''; ?>">API Details</a>  
                <a href="?page=my-menu-slug&tab=tab_two" class="nav-tab <?php echo $active_tab == 'tab_two' ? 'nav-tab-active' : ''; ?>">Contact Info</a>  
                <a href="?page=my-menu-slug&tab=tab_four" class="nav-tab <?php echo $active_tab == 'tab_four' ? 'nav-tab-active' : ''; ?>">Social Media</a>  
                <a href="?page=my-menu-slug&tab=tab_three" class="nav-tab <?php echo $active_tab == 'tab_three' ? 'nav-tab-active' : ''; ?>">Miscellaneous</a>  
            </h2>  

            
            <form method="post" action="options.php" style="display:none;"  > 
            <?php
                if( $active_tab == 'tab_one' ) {  

                    settings_fields( 'setting-group-1' );
                    do_settings_sections( 'my-menu-slug' );
                    submit_button();

                } elseif( $active_tab == 'tab_two' )  {

                    settings_fields( 'setting-group-2' );
                    do_settings_sections( 'my-menu-slug-1' );
                    submit_button();

                }
                elseif( $active_tab == 'tab_three' )  {

                    settings_fields( 'setting-group-3' );
                    do_settings_sections( 'my-menu-slug-3' );
                    submit_button();

                }
                elseif( $active_tab == 'tab_four' )  {

                    settings_fields( 'setting-group-4' );
                    do_settings_sections( 'my-menu-slug-4' );
                    submit_button();

                }
                

               
            ?>

                <?php //submit_button(); ?> 
            </form> 

        </div>
        <?php
}




function retailer_product_data_html(){
    
    ?>
    <div id="wpcontent1" class="client_info_wrap">
  
       
  <table class="form-table table-bordered table-hover widefat striped">
      <tbody>
          <tr>
              <th colspan="5">
                  <h4>Retailer Flooring Data</h4>
              </th>
          </tr>
           <tr>
               <th class="form-group" style="width: 10px;">#</th>
               <th class="form-group">Product Type</th>
               <th class="form-group">Brand</th>
               <!-- <th class="form-group">Deal Brand</th> -->
               <th class="form-group">Last Sync</th>
               <th class="form-group">Action</th>
           </tr>

              <?php
                   $product_json =  json_decode(get_option('product_json'));        
                  // write_log( $product_json);               
                   $brandmapping = array(
                       "carpet"=>"carpeting",
                       "hardwood"=>"hardwood_catalog",
                       "laminate"=>"laminate_catalog",
                       "lvt"=>"luxury_vinyl_tile",
                       "tile"=>"tile_catalog",
                       "waterproof"=>"solid_wpc_waterproof"
                   );
                   for($i=0;$i < count($product_json);$i++){
                       if($product_json[$i]->productType != 'rugs' ){
                       ?>
                       <tr class="hover-table">
                           <form name="productfrm" action="/wp-admin/admin.php?page=retailer_product_data" method="POST">
                               <td class="form-group" ><?php echo $i+1;?></td>
                               <td class="form-group"><?php echo ucfirst($product_json[$i]->productType); ?></td>
                               <?php
                               
                                   if( strcmp($product_json[$i]->productType,"Carpet")){
                                       ?>
                                       <input type="hidden" name="api_product_data_category" value="<?php if (isset($brandmapping[$product_json[$i]->productType])) {echo $pro_type = $brandmapping[$product_json[$i]->productType];} ?>" />
                                       <input type="hidden" name="api_product_data_brand" value="<?php echo $product_json[$i]->manufacturer;?>" />
                                       <?php
                                   }
                               ?>
                               <td class="form-group"><?php if (isset($product_json[$i]->brand)) { echo $product_json[$i]->brand; }?> <?php  if (isset($product_json[$i]->manufacturer)) { echo $product_json[$i]->manufacturer;} ?></td>
                               <!-- <td class="form-group"><?php // if (isset($product_json[$i]->dealerBrand)) { echo $product_json[$i]->dealerBrand; }?></td> -->
                               <td class="form-group"><?php

                                global $wpdb;    
                                $product_sync_table = $wpdb->prefix."sfn_sync";  
                                $quer_status =  "SELECT * FROM {$product_sync_table} WHERE product_brand = '{$product_json[$i]->manufacturer}' and  product_category = '{$pro_type}'" ;
                                $result_brand = $wpdb->get_results( $quer_status );    
                                
                                echo $result_brand[0]->sync_status.'==>';
                                      
                                        if( $result_brand){

                                            echo changeTimeZone($result_brand[0]->syn_datetime, "America/New_York", "Asia/Kolkata");
                                        }
                                        
                                 ?>
                               </td>
                               <td class="form-group"><button id="syncdata-pro" type="submit" class="button button-primary">Sync</button></td>
                           </form>
                       </tr>


                       <?php
                        }
                   }

               ?>
       </tbody>
  </table>

</div>
       <?php

                /**  Product Sync Mannual Trigger function **/

       if(isset($_POST['api_product_data_category'] ) && isset($_POST['api_product_data_brand'] )){

       new Example_Background_Processing();
        
        $upload = wp_upload_dir();
        $upload_dir = $upload['basedir'];
        $upload_dir = $upload_dir . '/sfn-data';
        if (! is_dir($upload_dir)) {
            mkdir( $upload_dir, 0700 );
        } 
     
        $sfnapi_category_array = array_keys($brandmapping,$_POST['api_product_data_category']);
        $sfnapi_category = $sfnapi_category_array[0];
        
        $permfile = $upload_dir.'/'.$_POST['api_product_data_category'].'_'.$_POST['api_product_data_brand'].'.csv';    
        $res = SOURCEURL.get_option('SITE_CODE').'/www/'.$sfnapi_category.'/'.$_POST['api_product_data_brand'].'.csv?'.SFN_STATUS_PARAMETER;

       $tmpfile = download_url( $res, $timeout = 900 );

             //var_dump($tmpfile);
            if(is_file($tmpfile)){               

                copy( $tmpfile, $permfile );
                unlink( $tmpfile ); 
                require_once plugin_dir_path( __FILE__ ) . 'example-plugin.php';            
                $data_url =   admin_url( '/admin.php?page=retailer_product_data&process=allcsv&main_category='.$_POST['api_product_data_category'].'&product_brand='.$_POST['api_product_data_brand'].'');
                write_log($data_url);

             }
             else{
                 
                $_SESSION['error'] = "Couldn't find products for this category";
                
            }     

     
  ?>
  <script>
  window.location.href = "<?php echo $data_url; ?>";
  </script>
  <?php exit();
  write_log($_GET);
}
}
   
if(isset($_GET['copy']) && $_GET['copy'] == 1){
    
    echo get_template_directory_uri()."-child";
    var_dump(xcopy("/var/www/html/wp-content/plugins/grand-child/fl-builder","/var/www/html/wp-content/themes/bb-theme-child/fl-builder"));
    exit;
}

/**
 * Copy a file, or recursively copy a folder and its contents
 * @author      Aidan Lister <aidan@php.net>
 * @version     1.0.1
 * @link        http://aidanlister.com/2004/04/recursively-copying-directories-in-php/
 * @param       string   $source    Source path
 * @param       string   $dest      Destination path
 * @param       int      $permissions New folder creation permissions
 * @return      bool     Returns true on success, false on failure
 */
function xcopy($source, $dest, $permissions = 0755)
{
    // Check for symlinks
    if (is_link($source)) {
        return symlink(readlink($source), $dest);
    }

    // Simple copy for a file
    if (is_file($source)) {
        return copy($source, $dest);
    }

    // Make destination directory
    if (!is_dir($dest)) {
        mkdir($dest, $permissions);
    }

    // Loop through the folder
    $dir = dir($source);
    
    while (false !== $entry = $dir->read()) {
        // Skip pointers
        if ($entry == '.' || $entry == '..') {
            continue;
        }

        // Deep copy directories
        xcopy("$source/$entry", "$dest/$entry", $permissions);
    }

    // Clean up
    $dir->close();
    return true;
}


function compare_some_objects($a, $b) { 
    return $b->order - $a->order;
}

function compare_cde_some_objects($a, $b) {     
    return $a->slider->order - $b->slider->order;
}

function compare_sale_some_objects($a, $b) {     
    return $a['days'] - $b['days'];
}


/**  Array Filter function **/

function getArrayFiltered($aFilterKey, $aFilterValue, $array) {
    $filtered_array = array();
    foreach ($array as $value) {
        if (isset($value->$aFilterKey)) {
            if ($value->$aFilterKey == $aFilterValue) {
                $filtered_array[] = $value;
            }
        }
    }

    return $filtered_array;
}


// Define path and URL to the ACF plugin.
define( 'MY_ACF_PATH', WP_PLUGIN_DIR . '/advanced-custom-fields-pro/' );
define( 'MY_ACF_URL', WP_PLUGIN_URL . '/advanced-custom-fields-pro/' );

// Include the ACF plugin.
include_once( MY_ACF_PATH . '/acf.php' );

// Customize the url setting to fix incorrect asset URLs.
add_filter('acf/settings/url', 'my_acf_settings_url');
function my_acf_settings_url( $url ) {
    return MY_ACF_URL;
}

if( function_exists('acf_add_local_field_group') ):

    acf_add_local_field_group(array(
        'key' => 'group_5d37ea111be98',
        'title' => 'Coretec Colorwall Fields',
        'fields' => array(
            array(
                'key' => 'field_5d37eb655704b',
                'label' => 'Plank Dimensions',
                'name' => 'plank_dimensions',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5d37eb6a5704c',
                'label' => 'Sq. Ft./Carton',
                'name' => 'sq_ftcarton',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5d37eb7d5704d',
                'label' => 'Flooring Type',
                'name' => 'flooring_type',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5d37eb915704e',
                'label' => 'Edge Profile',
                'name' => 'edge_profile',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5d37ec205704f',
                'label' => 'Core',
                'name' => 'core',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5d37ec2c57050',
                'label' => 'Attached Underlayment',
                'name' => 'attached_underlayment',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5d37ec4c57051',
                'label' => 'Installation Method',
                'name' => 'installation_method',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5d37ec5757052',
                'label' => 'Installation Level',
                'name' => 'installation_level',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5d37ec6d57053',
                'label' => 'Residential Warranty',
                'name' => 'residential_warranty',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5d37ec7857054',
                'label' => 'Structure Warranty',
                'name' => 'structure_warranty',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5d37ec7f57055',
                'label' => 'Waterproof Warranty',
                'name' => 'waterproof_warranty',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5d37ec8357056',
                'label' => 'Petproof Warranty',
                'name' => 'petproof_warranty',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5d37ec9157057',
                'label' => 'Commercial Warranty',
                'name' => 'commercial_warranty',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5d37ec9d57058',
                'label' => 'Environment Certifications',
                'name' => 'environment_certifications',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5d3831f8e4449',
                'label' => 'Description',
                'name' => 'description',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'maxlength' => '',
                'rows' => '',
                'new_lines' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'luxury_vinyl_tile',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => true,
        'description' => '',
    ));
    
    endif;


//add_action( 'wp_loaded', 'add_colorwall' );


   //Adding Coretec Page(Without Coretec Colorwall page)
   $isCoretec=false;
   $product_json =  json_decode(get_option('product_json')); 
 
   foreach ($product_json as $data) {
       foreach ($data as $key => $value) {
        if($key=="manufacturer" &&   $value== "COREtec"){
              $isCoretec=true;
        }
       }     
   }
   $coretec_colorwall =  get_option('coretec_colorwall');

    //Checking Floorte collection

    $args_for_floorte =  array(
        "post_type" => "hardwood_catalog",
        "post_status" => "publish",
        "orderby" => "title",
        "order" => "ASC",
        "posts_per_page" => 4, 
        'meta_query'    => array(
            'relation' => 'AND',
            array(
                  'key'       => 'collection',
                  'value'     => 'Floorte Magnificent',
                  'compare'   => '=',
              )  
        )
      );
       
      $floorte_query = new WP_Query($args_for_floorte); 
      if($floorte_query->have_posts()) {
    
        $isFloorte = true;
    
      }

/**  Add Page for Coretec Colorwall /  Coretec Template **/

function add_colorwall(){

    $sfn_account =  get_option('sfn_account');
    
     //Adding Coretec Page(Without Coretec Colorwall page)
   $isCoretec=false;
   $product_json =  json_decode(get_option('product_json')); 
 
   foreach ($product_json as $data) {
       foreach ($data as $key => $value) {
        if($key=="manufacturer" &&   $value== "COREtec"){
              $isCoretec=true;
        }
       }     
   }
   $coretec_colorwall =  get_option('coretec_colorwall');

    //Checking Colorwall Products

    $check_style = get_page_by_title( 'Choose Your Style' );
    $check_color = get_page_by_title( 'Choose Your Color' );
    $check_colorwall = get_page_by_title( 'Coretec Colorwall' );
    $check_coretec = get_page_by_title( 'Coretec' );
    $vinyl_page = get_page_by_path( '/flooring/vinyl/', OBJECT, 'page' );

  
 
 
 if($isCoretec){

    
   // var_dump($check);
   // Adding Coretec Colorwall Page
   if($coretec_colorwall == '1'){

    if( is_null($check_colorwall)  ){
        // Gather post data.
           $post_contet_data = '[fl_builder_insert_layout slug="colorwall-final"]';   
    
        $my_post_wall = array(
            'post_title'    => 'Coretec Colorwall',
            'post_name'     => 'coretec-colorwall',
            'post_content'  => $post_contet_data,
            'post_type'     => 'page',               
            'post_status'   => 'publish',
            'post_parent'   => $vinyl_page->ID,
            'post_author'  => 1,               
            'comment_status' => 'closed',   // if you prefer
            'ping_status' => 'closed'
            
        );
    
        // Insert the post into the database.
       $add_cwid = wp_insert_post( $my_post_wall );  
    
      update_post_meta( $add_cwid, '_wp_page_template', 'coretec_colorwall.php' );
      update_post_meta( $add_cwid, '_fl_builder_enabled', '1' );
     
    }

     // Adding Coretec Colorwall - Choose Style page
    if( is_null($check_style)  ){
        // Gather post data.
    
        $check_colorwall = get_page_by_title( 'Coretec Colorwall' );
        
        $my_post_style = array(
            'post_title'    => 'Choose Your Style',
            'post_name'     => 'choose-your-style',
            'post_content'  => 'This is my post',
            'post_type'     => 'page',       
            'post_parent'   => $check_colorwall->ID,        
            'post_status'   => 'publish',
            'post_author'  => 1,               
            'comment_status' => 'closed',   // if you prefer
            'ping_status' => 'closed'
            
        );
    
        // Insert the post into the database.
       $add_sid = wp_insert_post( $my_post_style );
       update_post_meta( $add_sid, '_wp_page_template', 'choose_your_style.php' );
    }
      
    // Adding Coretec Colorwall - Choose color page
    if( is_null($check_color)  ){
    
        $check_colorwall = get_page_by_title( 'Coretec Colorwall' );
    
        $my_post_color = array(
            'post_title'    => 'Choose Your Color',
            'post_name'     => 'choose-your-color',
            'post_content'  => 'This is my post',
            'post_type'     => 'page',    
            'post_parent'   => $check_colorwall->ID,               
            'post_status'   => 'publish',
            'post_author'  => 1,               
            'comment_status' => 'closed',   // if you prefer
            'ping_status' => 'closed'
            
        );
        
         // Insert the post into the database.
       $add_cid = wp_insert_post( $my_post_color );
        
       update_post_meta( $add_cid, '_wp_page_template', 'choose_your_color.php' );
       
     }

} else {

    if( is_null($check_coretec)  ){
    // Gather post data.
       $post_contet_data_core = '[fl_builder_insert_layout slug="coretec-no-colorwall"]';   

    $my_post_coretec = array(
        'post_title'    => 'Coretec',
        'post_name'     => 'coretec',
        'post_content'  => $post_contet_data_core,
        'post_type'     => 'page',               
        'post_status'   => 'publish',
        'post_parent'   => $vinyl_page->ID,
        'post_author'  => 1,               
        'comment_status' => 'closed',   // if you prefer
        'ping_status' => 'closed'
        
    );

    // Insert the post into the database.
   $add_corewid = wp_insert_post( $my_post_coretec );  

  update_post_meta( $add_corewid, '_wp_page_template', 'coretec_no_colorwall.php' );
  update_post_meta( $add_corewid, '_fl_builder_enabled', '1' );
 
}
    
  
}
 }

} 

/**  Move Page Template files to child theme directory **/

function file_replace() {

    $plugin_dir_land_fl = plugin_dir_path( __FILE__ ) . 'fl-builder/modules/content-slider/includes/frontend.php';
    $theme_dir_land_fl = get_stylesheet_directory() . '/fl-builder/modules/content-slider/includes/frontend.php';

    $plugin_dir_land = plugin_dir_path( __FILE__ ) . 'product-listing-templates/coretec_colorwall.php';
    $theme_dir_land = get_stylesheet_directory() . '/coretec_colorwall.php';

    $plugin_dir_noland = plugin_dir_path( __FILE__ ) . 'product-listing-templates/coretec_no_colorwall.php';
    $theme_dir_noland = get_stylesheet_directory() . '/coretec_no_colorwall.php';

     $plugin_dir_color = plugin_dir_path( __FILE__ ) . 'product-listing-templates/choose_your_color.php';
     $theme_dir_color = get_stylesheet_directory() . '/choose_your_color.php';

     $plugin_dir_style = plugin_dir_path( __FILE__ ) . 'product-listing-templates/choose_your_style.php';
     $theme_dir_style = get_stylesheet_directory() . '/choose_your_style.php';

     //Floorte
     $plugin_dir_floorte = plugin_dir_path( __FILE__ ) . 'product-listing-templates/floorte_hardwood.php';
     $theme_dir_floorte = get_stylesheet_directory() . '/floorte_hardwood.php';

     //covid
     $plugin_dir_covid = plugin_dir_path( __FILE__ ) . 'product-listing-templates/covid_template.php';
     $theme_dir_covid = get_stylesheet_directory() . '/covid_template.php';

    if (!copy($plugin_dir_land, $theme_dir_land)) {
        echo "failed to copy $plugin_dir_land to $theme_dir_land...\n";
    }
    if (!copy($plugin_dir_land_fl, $theme_dir_land_fl)) {
        echo "failed to copy $plugin_dir_land_fl to $theme_dir_land_fl...\n";
    }
    if (!copy($plugin_dir_noland, $theme_dir_noland)) {
        echo "failed to copy $plugin_dir_noland to $theme_dir_noland...\n";
    }

    if (!copy($plugin_dir_color, $theme_dir_color)) {
        echo "failed to copy $plugin_dir_color to $theme_dir_color...\n";
    }

    if (!copy($plugin_dir_style, $theme_dir_style)) {
        echo "failed to copy $plugin_dir_style to $theme_dir_style...\n";
    }

    //Floorte
    if (!copy($plugin_dir_floorte, $theme_dir_floorte)) {
        echo "failed to copy $plugin_dir_floorte to $theme_dir_floorte...\n";
    }

    //Covid
    if (!copy($plugin_dir_covid, $theme_dir_covid)) {
        echo "failed to copy $plugin_dir_covid to $theme_dir_covid...\n";
    }
}

add_action( 'wp_loaded', 'file_replace' );


add_action( 'sfn_bb_autoimport_hook', 'autoimport_caller' );
function autoimport_caller() {
    autoimport();
    floorte_autoimport();
    covid_autoimport();
    add_covid();
    add_colorwall();
    add_floorte();
   // wp_mail( 'devteam.agency@gmail.com', 'Auto Import function Called for '. get_bloginfo(), 'WP cron run at '.date("Y-m-d h:i:s",time()) );
}

/**  Auto Import Function for Coretec Colorwall /  Coretec Template **/

function autoimport() {
   
    // get the file
    require_once plugin_dir_path( __FILE__ ) . '/autoimport/autoimporter.php';

    if ( ! class_exists( 'Auto_Importer' ) ){
        die( 'Auto_Importer not found' );
    }
    // call the function
    $args = array(
        'file'        => plugin_dir_path( __FILE__ ) . '/autoimport/coretec_colorwall.xml'        
    );

 
    if(get_page_by_title('Colorwall Final',OBJECT,'fl-builder-template') == '' && get_page_by_title('Coretec � No Colorwall',OBJECT,'fl-builder-template') == '' &&  get_page_by_title('COLORWALL DESKTOP BANNER',OBJECT,'fl-builder-template') == '' &&  get_page_by_title('COLORWALL MOBILE',OBJECT,'fl-builder-template')==''){

        auto_import( $args );

}
}

/** Auto Import Function for Floorte Template **/

function floorte_autoimport() {

    require_once plugin_dir_path( __FILE__ ) . '/autoimport/autoimporter.php';

    if ( ! class_exists( 'Auto_Importer' ) ){
        die( 'Auto_Importer not found' );
    }
    // call the function
    $args = array(
        'file'        => plugin_dir_path( __FILE__ ) . '/autoimport/floorte.xml'        
    );

 
     if(get_page_by_title('Floorte Hardwood Template',OBJECT,'fl-builder-template') =='') {
       
        auto_import( $args );
    
    }
}


// Simple helper function for make menu item objects

function _custom_nav_menu_item( $title, $url, $order, $parent = 0 ){
    $item = new stdClass();
    $item->ID = 1000005 + $order;
    $item->db_id = $item->ID;
    $item->title = $title;
    $item->url = $url;
    $item->menu_order = $order;
    $item->menu_item_parent = $parent;
    $item->type = '';
    $item->object = '';
    $item->object_id = '';
    $item->classes = array();
    $item->target = '';
    $item->attr_title = '';
    $item->description = '';
    $item->xfn = '';
    $item->status = '';
    return $item;
  }
 

  /** Add Coretec Colorwall / Coretec menu if page exists**/

  if($isCoretec){
    add_filter( 'wp_get_nav_menu_items', 'custom_nav_menu_items', 20, 2 );
  }


function custom_nav_menu_items( $items, $menu ){

    $itemNew =  array();
     $i=0; 

    $coretec_colorwall =  get_option('coretec_colorwall');
    $isCoretec=false;
    $menu_url = home_url().'/flooring/vinyl/products/';  
    $menu_custom_url = ''; 

      foreach ($items as $item){ 
        $itemNew =   $items;        
        if($menu_url == $item->url ){

            $post_parent = $item->post_parent;            
            $menu_order = $item->menu_order + 1;
            $menu_item_parent = $item->menu_item_parent;
            
            $product_json =  json_decode(get_option('product_json')); 
          
            foreach ($product_json as $data) {
                foreach ($data as $key => $value) {
                    if($key=="manufacturer" &&   $value== "COREtec"){
                       $isCoretec=true;
                    }
                }     
            }
           

            
                // var_dump($check);
                // Adding Coretec Colorwall Page
                if($coretec_colorwall == '1'){

                    $menu_custom_url = home_url().'/flooring/vinyl/coretec-colorwall/';
                    $lable= "Coretec Colorwall";

                }elseif($isCoretec==true && $coretec_colorwall != '1'){
                    $menu_custom_url = home_url().'/flooring/vinyl/coretec/';
                    $lable= "Coretec";
                }
            $items_cust = array(_custom_nav_menu_item( $lable,$menu_custom_url,$menu_order, $menu_item_parent )); 
            
            array_splice( $items, $item->menu_order, 0, $items_cust );

           $itemNew =  $items;
           // $itemNew = array_slice($items, 0,$item->menu_order-1, true) +
           
           // array($items_cust) +  array_slice($items, $item->menu_order-1, count($items) - 1, true) ;       
            
        
            break;  
        }
       
       
      }

      if($coretec_colorwall == '1'){
        $menu_custom_url = home_url().'/flooring/vinyl/coretec-colorwall/';
      }elseif($isCoretec==true && $coretec_colorwall != '1'){
            $menu_custom_url = home_url().'/flooring/vinyl/coretec/';
     }
      $isItemFound=false;
     
      foreach ($itemNew as $item){     
      
        if($menu_custom_url == $item->url ){        
            $i =  $item->menu_order;        
            $isItemFound= true; 
            
          
        }
        if($isItemFound){
                                  
            $item->menu_order = $i;
            $i++;   
        }

      }    



  return $itemNew;
}
/**Add Floorte Page if floorte prodduct exist **/

//add_action( 'wp_loaded', 'add_floorte' );

function add_floorte(){

    $isFloorte = false;

    //Checking Floorte collection

   $args_for_floorte =  array(
    "post_type" => "hardwood_catalog",
    "post_status" => "publish",
    "orderby" => "title",
    "order" => "ASC",
    "posts_per_page" => 4, 
    'meta_query'    => array(
        'relation' => 'AND',
        array(
              'key'       => 'collection',
              'value'     => 'Floorte Magnificent',
              'compare'   => '=',
          )  
    )
  );
   
  $floorte_query = new WP_Query($args_for_floorte); 
  if($floorte_query->have_posts()) {

    $isFloorte = true;

  }

  
  //Floorte Hardwood page

  $check_floorte = get_page_by_title( 'Floorte Hardwood' );
  $hardwood_page = get_page_by_path( '/flooring/hardwood/', OBJECT, 'page' );
  $city = do_shortcode('[Retailer "city"]');
  $state = do_shortcode('[Retailer "state"]');
  $storename = do_shortcode('[Retailer "companyname"]');
  $metatitle = "Floorte: Waterproof Hardwood in ".$city.", ".$state." | ".$storename;
  $metadesc = "Floort� is 100% waterproof hardwood flooring that works as hard as you do. Explore styles in and around ".$city.", ".$state." | ".$storename; 
if( $isFloorte == 'true' && $check_floorte ==''  ){

    
    // Gather post data.
    $post_contet_data_floorte = '[fl_builder_insert_layout slug="floorte-hardwood-template"]';   
      
    $floorte_post = array(
        'post_title'    => 'Floorte Hardwood',
        'post_name'     => 'floorte-hardwood',
        'post_content'  => $post_contet_data_floorte,
        'post_type'     => 'page',               
        'post_status'   => 'publish',
        'post_parent'   => $hardwood_page->ID,
        'post_author'  => 1,               
        'comment_status' => 'closed',   // if you prefer
        'ping_status' => 'closed'
        
    );
  
    // Insert the post into the database.
   $add_floorte = wp_insert_post( $floorte_post );  
  
  update_post_meta( $add_floorte, '_wp_page_template', 'floorte_hardwood.php' );
  update_post_meta( $add_floorte, '_fl_builder_enabled', '1' );
  update_post_meta( $add_floorte, '_yoast_wpseo_title', $metatitle );
  update_post_meta( $add_floorte, '_yoast_wpseo_metadesc', $metadesc );
   
  }
  $check_floorte_page = get_page_by_title( 'Floorte Hardwood' );
  if($check_floorte_page){
      
      
    update_post_meta( $check_floorte_page->ID, '_yoast_wpseo_title', $metatitle );
    update_post_meta( $check_floorte_page->ID, '_yoast_wpseo_metadesc', $metadesc );
      
    }

}


//if($isFloorte){
 //    add_filter( 'wp_get_nav_menu_items', 'custom_nav_menu_items_floorte', 30, 2 );
 // }

/** Add Menu for Floorte **/
function custom_nav_menu_items_floorte( $items, $menu ){

    $itemNew =  array();
     $i=0;  
    // echo count($items);
     
      $menu_hard_url = home_url().'/flooring/hardwood/products/';  
      foreach ($items as $item){ 
        if($menu_hard_url == $item->url ){

               
            $post_parent = $item->post_parent;            
            $menu_order = $item->menu_order + 1;
            $menu_item_parent = $item->menu_item_parent;
            $menu_custom_url = home_url().'/flooring/hardwood/floorte-hardwood';
            $lable= "Floorte Hardwood";

            $items_cust = array(_custom_nav_menu_item( $lable,$menu_custom_url,$menu_order, $menu_item_parent )); 
            
            array_splice( $items, $item->menu_order, 0, $items_cust );

           $itemNew =  $items;

           break;  

        }

      }
      $isItemFound=false;
     
      foreach ($itemNew as $item){     
      
        if($menu_custom_url == $item->url ){        
            $i =  $item->menu_order;        
            $isItemFound= true; 
            
          
        }
        if($isItemFound){
                                  
            $item->menu_order = $i;
            $i++;   
        }

      }    
      return $itemNew;
}
    
//Function to add Meta Tags in Header for select in iphone
function add_meta_tags() {
    echo '<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">';
  }
  add_action('wp_head', 'add_meta_tags');
  //Function to add Meta Tags in Header without Plugin
  
  function sync_websiteinfo_cde($apiObj,$result){

    //API Call for getting website INFO
    
    $inputs = array('grant_type'=>'client_credentials','client_id'=>get_option('CLIENT_CODE'),'client_secret'=>get_option('CLIENTSECRET'));
    $headers = array('authorization'=>"bearer ".$result['access_token']);
    $website = $apiObj->call(BASEURL.get_option('SITE_CODE'),"GET",$inputs,$headers);

    $products = $apiObj->call(SOURCEURL.get_option('SITE_CODE')."/".PRODUCTURL,"GET",$inputs,$headers);
                
           
    if(isset($products) ){

        $product_json = json_encode($products);
        get_option('product_json')?update_option('product_json',$product_json):add_option('product_json',$product_json);
    }
    else{
        $msg =$contacts['message'];                
        $_SESSION['error'] = "Product Brand API";
        $_SESSION["error_desc"] =$msg;
    }

    //facebook domain verification

    $socialreport = $apiObj->call(BASEURL.get_option('SITE_CODE')."/".SOCIALURLREPORT,"GET",$inputs,$headers);

    if(isset($socialreport['success']) && $socialreport['success'] == 1 ){
        $socialreport_json =  json_encode($socialreport['result']);
        get_option('social_report') || get_option('social_report')==""?update_option('social_report',$socialreport_json):add_option('social_report',$socialreport_json);   
    }
    
    
    if(isset($website['success']) && $website['success'] ){
        $sfn_acc = $website['result']['sfn'];
        $coretec_color = $website['result']['options']['colorWall'];
        $covid = $website['result']['options']['covid'];
        $website_json = json_encode($website['result']);
       update_option('website_json',$website_json);
        update_option('sfn_account',$sfn_acc);
        update_option('coretec_colorwall',$coretec_color);
        
        if(isset($website['result']['sites'])){
            for($k=0;$k<count($website['result']['sites']);$k++){
                if($website['result']['sites'][$k]['instance'] == ENV){
                    update_option('blogname',$website['result']['sites'][$k]['name']);
                    $gtmid = $website['result']['sites'][$k]['gtmId'];
                    get_option( 'gtm_script_insert')?update_option( 'gtm_script_insert',$gtmid):update_option( 'gtm_script_insert',$gtmid);
                }
            }
        }
        //get_option( 'gtm_script_insert', )
        
    }
    else{
        $msg =$website['message'];                
        //echo $msg;
    }

  }

  ///remove multidimensional array element by value 
  function removeElementWithValue($array, $key, $value){
    foreach($array as $subKey => $subArray){
         if($subArray[$key] == $value){
              unset($array[$subKey]);
         }
    }
    return $array;
}

/** Carpet Mohawk Product sync Cron job **/
  
  add_action( 'sync_carpet_monday_event_mohawk', 'do_this_monday_carpet_mohawk', 10, 2 );

  function do_this_monday_carpet_mohawk() {    

    check_product_group_redirection();
     
    //  wp_mail( 'velocity.syncproduct@gmail.com',  get_bloginfo(). '- Carpet Mohawk Sync Started '.date("Y-m-d h:i:s",time()),'Carpet Product Sync Started' );
  
    
     global $wpdb;
     $upload = wp_upload_dir();
     $upload_dir = $upload['basedir'];
     $upload_dir = $upload_dir . '/sfn-data';  
     $table_posts = $wpdb->prefix.'posts';
	$table_meta = $wpdb->prefix.'postmeta';	

    $sql_delete = "DELETE FROM $table_meta WHERE meta_key = 'endpoint' " ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);	
     
     write_log('Carpet batch mohawk sync started');
  
     $permfile = $upload_dir.'/carpeting_mohawk.json';
     //$res = SOURCEURL.get_option('SITE_CODE').'/www/carpet/'.$carpet->manufacturer.'.json?status=active&status=pending&status=dropped&status=gone&rpp=100&pg='.$x;
     $res = SOURCEURL.get_option('SITE_CODE').'/www/carpet/Mohawk.json?'.SFN_STATUS_PARAMETER;

     write_log($res);
     
     $tmpfile = download_url( $res, $timeout = 900 );
      write_log($tmpfile);
  
     if(is_file($tmpfile)){
  
         copy( $tmpfile, $permfile );
         unlink( $tmpfile ); 
         Write_log('file downloaded');
     }
     
     Write_log('file downloaded path -'.$permfile);
  
     $strJsonFileContents = file_get_contents($permfile);
    
     $handle = json_decode($strJsonFileContents, true); 
     
  
     if (empty($handle)) {

        switch (json_last_error()) {
			case JSON_ERROR_NONE:
			  write_log("No errors");
			  break;
			case JSON_ERROR_DEPTH:
                write_log( "Maximum stack depth exceeded");
			  break;
			case JSON_ERROR_STATE_MISMATCH:
                write_log( "Invalid or malformed JSON");
			  break;
			case JSON_ERROR_CTRL_CHAR:
                write_log( "Control character error");
			  break;
			case JSON_ERROR_SYNTAX:
                write_log( "Syntax error");
			  break;
			case JSON_ERROR_UTF8:
                write_log( "Malformed UTF-8 characters");
			  break;
			default:
              write_log( "Unknown error");
			  break;
		  }	
  
     }else{   
  
      write_log('auto_sync - do_this_monday_carpet_mohawk - mohawk');
      $obj = new Example_Background_Processing();
      set_time_limit(0);
      $obj->handle_all('carpeting', 'mohawk');
      write_log('Carpet mohawk batch sync ended');
  
     } 
     
     write_log('Sync Completed for mohawk Carpet brands');    

   //  compare_csv_onsite_products('carpeting','Mohawk');
  }


    /** Carpet Shaw Product sync Cron job **/

  add_action( 'sync_carpet_tuesday_event_shaw', 'do_this_tuesday_carpet_shaw', 10, 2 );

  function do_this_tuesday_carpet_shaw() {    

   
     global $wpdb;
     $upload = wp_upload_dir();
     $upload_dir = $upload['basedir'];
     $upload_dir = $upload_dir . '/sfn-data';
     $table_posts = $wpdb->prefix.'posts';
	 $table_meta = $wpdb->prefix.'postmeta';	

     $sql_delete = "DELETE FROM $table_meta WHERE meta_key = 'endpoint' " ;						
	 $delete_endpoint = $wpdb->get_results($sql_delete);	

     write_log('Carpet batch shaw sync started');
  
     $permfile = $upload_dir.'/carpeting_shaw.json';
     //$res = SOURCEURL.get_option('SITE_CODE').'/www/carpet/'.$carpet->manufacturer.'.json?status=active&status=pending&status=dropped&status=gone&rpp=100&pg='.$x;
     $res = SOURCEURL.get_option('SITE_CODE').'/www/carpet/Shaw.json?'.SFN_STATUS_PARAMETER;

     write_log($res);
     
     $tmpfile = download_url( $res, $timeout = 900 );
      write_log($tmpfile);
  
     if(is_file($tmpfile)){
  
         copy( $tmpfile, $permfile );
         unlink( $tmpfile ); 
         Write_log('file downloaded');
     }
     
     Write_log('file downloaded path -'.$permfile);
  
     $strJsonFileContents = file_get_contents($permfile);
    
     $handle = json_decode($strJsonFileContents, true); 
     
  
     if (empty($handle)) {

        switch (json_last_error()) {
			case JSON_ERROR_NONE:
			  write_log("No errors");
			  break;
			case JSON_ERROR_DEPTH:
                write_log( "Maximum stack depth exceeded");
			  break;
			case JSON_ERROR_STATE_MISMATCH:
                write_log( "Invalid or malformed JSON");
			  break;
			case JSON_ERROR_CTRL_CHAR:
                write_log( "Control character error");
			  break;
			case JSON_ERROR_SYNTAX:
                write_log( "Syntax error");
			  break;
			case JSON_ERROR_UTF8:
                write_log( "Malformed UTF-8 characters");
			  break;
			default:
              write_log( "Unknown error");
			  break;
		  }	
  
     }else{   
  
      write_log('auto_sync - do_this_tuesday_carpet_shaw - shaw');
      $obj = new Example_Background_Processing();
      set_time_limit(0);
      $obj->handle_all('carpeting', 'shaw');
      write_log('Carpet Shaw batch sync ended');
  
     } 
    
    
     write_log('Sync Completed for shaw Carpet brands');    

    //  compare_csv_onsite_products('carpeting','Shaw');
  }

  /** Carpet Dreamweaver Product sync Cron job **/

  add_action( 'sync_carpet_saturday_event_dreamweaver', 'do_this_saturday_carpet_dreamweaver', 10, 2 );

  function do_this_saturday_carpet_dreamweaver() {        
     
  
     global $wpdb;
     $upload = wp_upload_dir();
     $upload_dir = $upload['basedir'];
     $upload_dir = $upload_dir . '/sfn-data';
     $product_json =  json_decode(get_option('product_json'));     
     $c_array = getArrayFiltered('productType','carpet',$product_json);    

    $table_posts = $wpdb->prefix.'posts';
	$table_meta = $wpdb->prefix.'postmeta';	

    $sql_delete = "DELETE FROM $table_meta WHERE meta_key = 'endpoint' " ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);	

     $carpet_array = json_decode(json_encode($c_array), True);

     $carpet_array = removeElementWithValue($carpet_array, "manufacturer", 'Mohawk');
     $carpet_array = removeElementWithValue($carpet_array, "manufacturer", 'Shaw');         


     foreach ($carpet_array as $carpet){
       
        if($carpet['manufacturer'] == 'dixiehome'){

            $carpet_manufacturer = 'Dixie Home';
    
        }elseif($carpet['manufacturer'] == 'dreamweaver'){
    
            $carpet_manufacturer = 'Dream Weaver';
    
        }else{
    
            $carpet_manufacturer = ucfirst($carpet['manufacturer']);
    
        }     
            
  
     write_log('Carpet batch '.$carpet['manufacturer'].' sync started');  
  
     $permfile = $upload_dir.'/carpeting_'.$carpet['manufacturer'].'.json';
    
     $res = SOURCEURL.get_option('SITE_CODE').'/www/carpet/'.$carpet['manufacturer'].'.json?'.SFN_STATUS_PARAMETER;

     write_log($res);
     
     $tmpfile = download_url( $res, $timeout = 900 );
      write_log($tmpfile);
  
     if(is_file($tmpfile)){
  
         copy( $tmpfile, $permfile );
         unlink( $tmpfile ); 
         Write_log('file downloaded');
     }
     
     Write_log('file downloaded path -'.$permfile);
  
     $strJsonFileContents = file_get_contents($permfile);
    
     $handle = json_decode($strJsonFileContents, true); 
     
  
     if (empty($handle)) {

        switch (json_last_error()) {
            case JSON_ERROR_NONE:
              write_log("No errors");
              break;
            case JSON_ERROR_DEPTH:
                write_log( "Maximum stack depth exceeded");
              break;
            case JSON_ERROR_STATE_MISMATCH:
                write_log( "Invalid or malformed JSON");
              break;
            case JSON_ERROR_CTRL_CHAR:
                write_log( "Control character error");
              break;
            case JSON_ERROR_SYNTAX:
                write_log( "Syntax error");
              break;
            case JSON_ERROR_UTF8:
                write_log( "Malformed UTF-8 characters");
              break;
            default:
              write_log( "Unknown error");
              break;
          }

     }else{   
  
      write_log('auto_sync - do_this_saturday_carpet_dreamweaver - '.$carpet['manufacturer']);
      $obj = new Example_Background_Processing();
      set_time_limit(0);
      $obj->handle_all('carpeting', $carpet['manufacturer']);
      write_log('Carpet '.$carpet['manufacturer'].' batch sync ended');
  
     }      
  
    // compare_csv_onsite_products('carpeting',$carpet_manufacturer);
}
    

     write_log('Sync Completed for all other Carpet brands');    
  }


  /** Hardwood Product sync Cron job **/

add_action( 'sync_hardwood_wednesday_event', 'do_this_wednesday_hardwood', 10, 2 );

function do_this_wednesday_hardwood() {   
    
    check_product_group_redirection();

    write_log('sync_tue'); 
    $product_json =  json_decode(get_option('product_json'));     
    $hardwood_array = getArrayFiltered('productType','hardwood',$product_json);
    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';  
    
    $table_posts = $wpdb->prefix.'posts';
	$table_meta = $wpdb->prefix.'postmeta';	

    $sql_delete = "DELETE FROM $table_meta WHERE meta_key = 'endpoint' " ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);	
  
   foreach ($hardwood_array as $hardwood){
  

    $permfile = $upload_dir.'/hardwood_catalog_'.$hardwood->manufacturer.'.json';
    $res = SOURCEURL.get_option('SITE_CODE').'/www/hardwood/'.$hardwood->manufacturer.'.json?'.SFN_STATUS_PARAMETER;
    
    $tmpfile = download_url( $res, $timeout = 900 );

    if(is_file($tmpfile)){
        copy( $tmpfile, $permfile );
        unlink( $tmpfile ); 
    } 

    write_log('auto_sync - do_this_wednesday_hardwood-'.$hardwood->manufacturer);
    $obj = new Example_Background_Processing();
    $obj->handle_all('hardwood_catalog', $hardwood->manufacturer);

    write_log('Sync Completed - '.$hardwood->manufacturer);
    
    }
   
    write_log('Sync Completed for all hardwood brand');

    //compare_csv_onsite_products('hardwood_catalog');

}

/** Laminate Product sync Cron job **/

add_action( 'sync_laminate_thursday_event', 'do_this_thursday_laminate', 10, 2 );

function do_this_thursday_laminate() {

    write_log('Sync Started');
    $product_json =  json_decode(get_option('product_json'));     
    $laminate_array = getArrayFiltered('productType','laminate',$product_json);
    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';   
    $table_posts = $wpdb->prefix.'posts';
	$table_meta = $wpdb->prefix.'postmeta';	

    $sql_delete = "DELETE pm  FROM $table_posts p
                INNER  JOIN $table_meta pm ON pm.post_id = p.ID
                WHERE p.post_type = 'laminate_catalog' and pm.meta_key='endpoint'" ;	

	$delete_endpoint = $wpdb->get_results($sql_delete);		
    
    
   foreach ($laminate_array as $laminate){
    write_log('brand');
    write_log( $laminate->manufacturer);

   //exit;

    $permfile = $upload_dir.'/laminate_catalog_'.$laminate->manufacturer.'.json';
    $res = SOURCEURL.get_option('SITE_CODE').'/www/laminate/'.$laminate->manufacturer.'.json?'.SFN_STATUS_PARAMETER;
    write_log($res);
    
    $tmpfile = download_url( $res, $timeout = 900 );

    if(is_file($tmpfile)){
        copy( $tmpfile, $permfile );
        unlink( $tmpfile ); 
    } 

    write_log('auto_sync - sync_laminate_thursday_event-'.$laminate->manufacturer);
    $obj = new Example_Background_Processing();
    $obj->handle_all('laminate_catalog', $laminate->manufacturer);   
    write_log('Sync Completed - '.$laminate->manufacturer);  
    
    }  
    write_log('Sync Completed for all Laminate brand');    

  //  compare_csv_onsite_products('laminate_catalog');

}


/** LVT Product sync Cron job **/


add_action( 'sync_lvt_thursday_event', 'do_this_thursday_lvt', 10, 2 );

function do_this_thursday_lvt() {

    
    $product_json =  json_decode(get_option('product_json'));     
    $lvt_array = getArrayFiltered('productType','lvt',$product_json);
    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';   
    
    $table_posts = $wpdb->prefix.'posts';
	$table_meta = $wpdb->prefix.'postmeta';	

    $sql_delete = "DELETE pm  FROM $table_posts p
                   INNER  JOIN $table_meta pm ON pm.post_id = p.ID
                   WHERE p.post_type = 'luxury_vinyl_tile' and pm.meta_key='endpoint'" ;

	$delete_endpoint = $wpdb->get_results($sql_delete);

   foreach ($lvt_array as $lvt){

    $permfile = $upload_dir.'/luxury_vinyl_tile_'.$lvt->manufacturer.'.json';
    $res = SOURCEURL.get_option('SITE_CODE').'/www/lvt/'.$lvt->manufacturer.'.json?'.SFN_STATUS_PARAMETER;
  
    $tmpfile = download_url( $res, $timeout = 900 );

    if(is_file($tmpfile)){
        copy( $tmpfile, $permfile );
        unlink( $tmpfile ); 
    }                
  

    write_log('auto_sync - sync_lvt_thursday_event-'.$lvt->manufacturer);
    $obj = new Example_Background_Processing();
    $obj->handle_all('luxury_vinyl_tile', $lvt->manufacturer);
   
    write_log('Sync Completed - '.$lvt->manufacturer);

    }  
    write_log('Sync Completed for all lvt brand');    

   // compare_csv_onsite_products('luxury_vinyl_tile');
    
}

/** Tile Product sync Cron job **/

add_action( 'sync_tile_friday_event', 'do_this_friday_tile', 10, 2 );

function do_this_friday_tile() {

    $product_json =  json_decode(get_option('product_json'));     
    $tile_array = getArrayFiltered('productType','tile',$product_json);
    global $wpdb;
    $upload = wp_upload_dir();
    $upload_dir = $upload['basedir'];
    $upload_dir = $upload_dir . '/sfn-data';  

    $table_posts = $wpdb->prefix.'posts';
	$table_meta = $wpdb->prefix.'postmeta';	

    $sql_delete = "DELETE FROM $table_meta WHERE meta_key = 'endpoint' " ;	
    
    
	$delete_endpoint = $wpdb->get_results($sql_delete);
    
    
   foreach ($tile_array as $tile){

    $permfile = $upload_dir.'/tile_catalog_'.$tile->manufacturer.'.json';
    $res = SOURCEURL.get_option('SITE_CODE').'/www/tile/'.$tile->manufacturer.'.json?'.SFN_STATUS_PARAMETER;
   
    $tmpfile = download_url( $res, $timeout = 900 );

    if(is_file($tmpfile)){
        copy( $tmpfile, $permfile );
        unlink( $tmpfile ); 
    }   
   

    write_log('auto_sync - sync_carpet_friday_event-'.$tile->manufacturer);
    $obj = new Example_Background_Processing();
    $obj->handle_all('tile_catalog', $tile->manufacturer);
   
    write_log('Sync Completed - '.$tile->manufacturer);    
    }         

    write_log('Sync Completed for all lvt brand');   

    //compare_csv_onsite_products('tile_catalog');
    
}



wp_clear_scheduled_hook( '301_task_hook' );
if (!wp_next_scheduled('301_task_hook_dropped')) {
    
         $interval =  getIntervalTime('friday');    
         wp_schedule_event( time() + $interval, 'each_saturday', '301_task_hook_dropped');
   }
   
  add_action( '301_task_hook_dropped', 'custom_301_redirects' ); 
 
 // custom 301 redirects from sfn dropped api
 function custom_301_redirects(){
    global $wpdb;
    $table_redirect = $wpdb->prefix.'redirection_items';
    $table = $wpdb->prefix.'redirection_groups';
    $table_posts = $wpdb->prefix.'posts';
	$table_meta = $wpdb->prefix.'postmeta';	

    write_log('In 301 function cron');
    check_product_group_redirection();
    
    $datum = $wpdb->get_results("SELECT * FROM $table WHERE name = 'Products'");
    $redirect_group =  $datum[0]->id;  

     $upload = wp_upload_dir();
     $upload_dir = $upload['basedir'];
     $upload_dir = $upload_dir . '/sfn-data';
     $permfile = $upload_dir.'/deleted_product.json';

     // Gives us access to the download_url() and wp_handle_sideload() functions
     require_once( ABSPATH . 'wp-admin/includes/file.php' );
  
     $res ='https://sfn.mm-api.agency/dropped/'.get_option('SITE_CODE');
     
     $tmpfile = download_url( $res, $timeout = 900 );

     $redirect_obj = array();

     $brandmapping = array(
        "/flooring/carpet/products/"=>"carpeting",
        "/flooring/hardwood/products/"=>"hardwood_catalog",
        "/flooring/laminate/products/"=>"laminate_catalog",
        "/flooring/"=>"luxury_vinyl_tile",
        "/flooring/tile/products/"=>"tile_catalog",
        "/flooring/waterproof/"=>"solid_wpc_waterproof"
    );

    $categorymapping = array(
        "carpeting"=>"carpet",
        "hardwood_catalog"=>"hardwood",
        "laminate_catalog"=>"laminate",
        "luxury_vinyl_tile"=>"lvt",
        "tile_catalog"=>"tile"       
    );
  
     if(is_file($tmpfile)){
  
      copy( $tmpfile, $permfile );
      unlink( $tmpfile ); 
    
  
      }
  
      $strJsonFileContents = file_get_contents($permfile);
  
      $handle = json_decode(($strJsonFileContents), true);     
  
        $d = 0;
          foreach($handle as $field => $value){ 

            if($value['category'] != 'rugs'){

           // sleep(1);
             
            write_log('sku----------------'.$value['sku']);

            $find_sku = $value['sku'];

            $pro_category = array_search($value['category'],$categorymapping);
          
            $sql_sku = "SELECT $table_meta.post_id 
						FROM $table_meta
						WHERE  $table_meta.meta_key = 'sku' 
						AND $table_meta.meta_value = '$find_sku'" ;	

               //         write_log($sql_sku);
         
            $query = $wpdb->get_results($sql_sku,ARRAY_A);		

            write_log('<------'.$d.'--------->');

            if (count($query) > 0){
                   
            write_log('Found Post in database>');

            }else{

                write_log('NO Post found');
            }

           // exit;
            
        if (count($query) > 0){

  
                  $post_id = $query[0]['post_id'];

                  write_log('posted id ->'.$post_id );   
                  write_log('SKU ->'.$value['sku'] );  
                  write_log('SKU ->'.$value['category'] );

                  $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$value['sku'].'%"');

                        if($checkalready == ''){ 
                                    
                            $source_url = wp_make_link_relative(get_permalink($post_id));
                            $match_url = rtrim($source_url, '/');

                            write_log($source_url);

                            $post_type =  get_post_type($post_id );

                            $destination_url = array_search($post_type,$brandmapping);

                            write_log($destination_url);

                            $data = array("url" => $source_url,
                                    "match_url" => $match_url,
                                    "match_data" => "",
                                    "action_code" => "301",
                                    "action_type" => "url",
                                    "action_data" => $destination_url,
                                    "match_type" => "url",
                                    "title" => $value['sku'],
                                    "regex" => "true",
                                    "group_id" => $redirect_group,
                                    "position" => "1",
                                    "last_access" => current_time( 'mysql' ),
                                    "status" => "enabled");

                                    $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

                                    $wpdb->insert($table_redirect,$data,$format);                                   
                                   

                                }        

                                if($post_id !=''){

                                    wp_delete_post($post_id);
                    
                                    write_log('Post Deleted form Database as per Dropped API');
                    
        }
     }
  
          

    }
    $d++;
}

     
  write_log('301 custom redirected updated from API is Ended');
  
  }

  function check_product_group_redirection(){
    global $wpdb;
    $table_redirect_group = $wpdb->prefix.'redirection_groups';
    $datum = $wpdb->get_results("SELECT * FROM $table_redirect_group WHERE name = 'Products'");
    if($wpdb->num_rows > 0) {

        write_log('check_product_group_redirection - Product Group Exists');

    }else{        

        $wpdb->insert($table_redirect_group, array(
            'name' => 'Products', //replaced non-existing variables $lq_name, and $lq_descrip, with the ones we set to collect the data - $name and $description
            'tracking' => '1',
            'module_id' => '1',
            'status' => 'enabled',
            'position' => '2'
            ),
            array( '%s','%s','%s','%s','%s') //replaced %d with %s - I guess that your description field will hold strings not decimals
        );

    }

  }

//301 redirect added from 404 logs table

if (!wp_next_scheduled('404_redirection_301_log_cronjob')) {
    
    $interval =  getIntervalTime('friday');    
    wp_schedule_event( time() +  17800, 'daily', '404_redirection_301_log_cronjob');
}

add_action( '404_redirection_301_log_cronjob', 'custom_404_redirect_hook' ); 



function check_404($url) {
   $headers=get_headers($url, 1);
   if ($headers[0]!='HTTP/1.1 200 OK') {return true; }else{ return false;}
}

 // custom 301 redirects from  404 logs table
 function custom_404_redirect_hook(){
    global $wpdb;    
    write_log('in function');

    $table_redirect = $wpdb->prefix.'redirection_items';
    $table_name = $wpdb->prefix . "redirection_404";
    $table_group = $wpdb->prefix.'redirection_groups';

     $data_404 = $wpdb->get_results( "SELECT * FROM $table_name" );
     $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
     $redirect_group =  $datum[0]->id;  

    if ($data_404)
    {      
      foreach ($data_404 as $row_404) 
       {            

        if (strpos($row_404->url,'carpet') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {
            write_log($row_404->url);      
            
           $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

           
            $destination_url_carpet = '/flooring/carpet/products/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_carpet,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            write_log( 'carpet 301 added ');
         }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');


         }

        }
        else if (strpos($row_404->url,'hardwood') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');
            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            $destination_url_hardwood = '/flooring/hardwood/products/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_hardwood,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }else{

            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

           }

            write_log( 'hardwood 301 added ');

        }
        else if (strpos($row_404->url,'laminate') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            $destination_url_laminate = '/flooring/laminate/products/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" =>$row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_laminate,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'laminate 301 added ');


        }
        else if ((strpos($row_404->url,'luxury-vinyl') !== false || strpos($row_404->url,'vinyl') !== false) && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url);  

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            
            if (strpos($row_404->url,'luxury-vinyl') !== false ){
                $destination_url_lvt = '/flooring/luxury-vinyl/products/';
            }elseif(strpos($row_404->url,'vinyl') !== false){
                $destination_url_lvt = '/flooring/';
            }
            
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" =>$row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_lvt,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');
            }

            write_log( 'luxury-vinyl 301 added ');
        }
        else if (strpos($row_404->url,'tile') !== false && strpos($row_404->url,'products') !== false && strpos($row_404->url,'flooring') !== false) {

            write_log($row_404->url); 

            $checkalready =  $wpdb->query('Select * FROM '.$table_redirect.' WHERE url LIKE "%'.$row_404->url.'%"');

            $url_404 = home_url().''.$row_404->url;
            $headers_res = check_404($url_404);

           if($checkalready == '' && $headers_res == 'false'){  

            $destination_url_tile = '/flooring/tile/products/';
            $match_url = rtrim($row_404->url, '/');

            $data = array("url" => $row_404->url,
            "match_url" => $match_url,
            "match_data" => "",
            "action_code" => "301",
            "action_type" => "url",
            "action_data" => $destination_url_tile,
            "match_type" => "url",
            "title" => "",
            "regex" => "true",
            "group_id" => $redirect_group,
            "position" => "1",
            "last_access" => current_time( 'mysql' ),
            "status" => "enabled");

            $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

            $wpdb->insert($table_redirect,$data,$format);
            $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }else{

                $wpdb->query('DELETE FROM '.$table_name.' WHERE id = "'.$row_404->id.'"');

            }

            write_log( 'tile 301 added ');
        }

       }  
    }

 }
 
  //Delete duplicate 301 redirects 

if (!wp_next_scheduled('301_redirection_duplicate_delete_cronjob')) {    
      
    wp_schedule_event( time() +  17800, 'daily', '301_redirection_duplicate_delete_cronjob');
}

add_action( '301_redirection_duplicate_delete_cronjob', 'Custom301_redirection_duplicate_delete' );

 // Delete duplicate 301 redirects entries
 function Custom301_redirection_duplicate_delete(){

    global $wpdb;    
    write_log('Delete duplicate 301 redirects in function');
    $table_redirect = $wpdb->prefix.'redirection_items';

    write_log($check_already);

    $check_already = $wpdb->query('SELECT url, COUNT(url) FROM '.$table_redirect.' GROUP BY url HAVING COUNT(url) > 1');

    write_log($check_already);

    $wpdb->query('DELETE t1 FROM '.$table_redirect.' t1 INNER JOIN '.$table_redirect.' t2 WHERE t1.id < t2.id AND t1.url= t2.url;');

    write_log('Deleted duplicate');

 }



/**Add Covid 19 Page if cde option is on **/

//add_action( 'wp_loaded', 'add_covid' );

function add_covid(){ 

  $iscovid  =  get_option('covid');
  $check_covid = get_page_by_title( 'Alert' );

  $storename = do_shortcode('[Retailer "companyname"]');
  $metatitle = "Health & Safety Announcement: COVID-19/Coronavirus | ".$storename;
  $metadesc = "At ".$storename." the COVID-19/coronavirus situation evolves and affects our communities we are committed to taking proper measures to protect the health and safety of our employees and customers."; 
 
if( $iscovid == '1' && $check_covid ==''  ){
    
    // Gather post data.
    $post_contet_data_covid = '[fl_builder_insert_layout slug="covid19-page-template"]';   
      
    $covid_post = array(
        'post_title'    => 'Alert',
        'post_name'     => 'alert',
        'post_content'  => $post_contet_data_covid,
        'post_type'     => 'page',               
        'post_status'   => 'publish',       
        'post_author'  => 1,               
        'comment_status' => 'closed',   // if you prefer
        'ping_status' => 'closed'
        
    );
  
    // Insert the post into the database.
   $add_covid = wp_insert_post( $covid_post );  
  
  update_post_meta( $add_covid, '_wp_page_template', 'covid_template.php' );
  update_post_meta( $add_covid, '_fl_builder_enabled', '1' );
  update_post_meta( $add_covid, '_yoast_wpseo_title', $metatitle );
  update_post_meta( $add_covid, '_yoast_wpseo_metadesc', $metadesc );
   
  }

  if( $iscovid == '0' || $iscovid == ''){

        $post_covid = array( 'ID' => $check_covid->ID, 'post_status' => 'draft' );
        wp_update_post($post_covid);

  }

}


/** Auto Import Function for covid19 Template **/

function covid_autoimport() {

    require_once plugin_dir_path( __FILE__ ) . '/autoimport/autoimporter.php';

    if ( ! class_exists( 'Auto_Importer' ) ){
        die( 'Auto_Importer not found' );
    }
    // call the function
    $args = array(
        'file'        => plugin_dir_path( __FILE__ ) . '/autoimport/covid-19_assets.xml'        
    );
       
    $check_old_covid = get_page_by_title('covid19-template',OBJECT,'fl-builder-template');
    if($check_old_covid != ''){

        wp_delete_post($check_old_covid->ID, true);
    }

    $check_old_covid_banner = get_page_by_title('covid19-banner',OBJECT,'fl-builder-template');
    if($check_old_covid_banner != ''){

        wp_delete_post($check_old_covid_banner->ID, true);
    }
 
     if(get_page_by_title('covid19-page-template',OBJECT,'fl-builder-template') =='') {
       
        auto_import( $args );
    
    }
}


add_action( 'GDPR_database_update_cronjob', 'gdpr_setting_databse' );

if( !wp_next_scheduled( 'GDPR_database_update_cronjob' ) ) {
    // Schedule the event
    
    wp_schedule_event(strtotime('09:00:00'), 'daily', 'GDPR_database_update_cronjob' );
}

function gdpr_setting_databse(){
global $wpdb;

write_log('gdpr_setting_databse');

$gdpr_table = $wpdb->prefix."gdpr_cc_options";

$sql_gdpr_delete = "DROP TABLE IF EXISTS $gdpr_table";

$wpdb->query($sql_gdpr_delete);

$sql_gdpr = "CREATE TABLE $gdpr_table (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `option_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1',
    `option_value` longtext CHARACTER SET utf8,
    `site_id` int(11) DEFAULT NULL,
    `extras` longtext CHARACTER SET utf8,
    PRIMARY KEY (`id`)
  ) DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;";

$wpdb->query($sql_gdpr);

$sql_gdpr_insert = "INSERT INTO $gdpr_table (`id`, `option_key`, `option_value`, `site_id`, `extras`) VALUES
(1,	'moove_gdpr_infobar_visibility',	'visible',	1,	NULL),
(2,	'moove_gdpr_reject_button_enable',	'0',	1,	NULL),
(3,	'moove_gdpr_close_button_enable',	'0',	1,	NULL),
(4,	'moove_gdpr_colour_scheme',	'1',	1,	NULL),
(5,	'moove_gdpr_nonce',	'6b2bbad3d4',	1,	NULL),
(6,	'_wp_http_referer',	'/wp-admin/admin.php?page=moove-gdpr&tab=branding',	1,	NULL),
(7,	'moove_gdpr_info_bar_content',	'<p>Our site uses cookies to improve your experience. By using our site, you acknowledge and accept our use of cookies.</p>\n',	1,	NULL),
(8,	'moove_gdpr_infobar_accept_button_label',	'Accept',	1,	NULL),
(9,	'moove_gdpr_infobar_reject_button_label',	'Reject',	1,	NULL),
(10,	'moove_gdpr_infobar_position',	'bottom',	1,	NULL),
(11,	'moove_gdpr_modal_powered_by_disable',	'0',	1,	NULL),
(12,	'moove_gdpr_plugin_layout',	'v1',	1,	NULL),
(13,	'moove_gdpr_modal_save_button_label',	'Save Changes',	1,	NULL),
(14,	'moove_gdpr_modal_allow_button_label',	'Enable All',	1,	NULL),
(15,	'moove_gdpr_modal_enabled_checkbox_label',	'Enabled',	1,	NULL),
(16,	'moove_gdpr_modal_disabled_checkbox_label',	'Disabled',	1,	NULL),
(17,	'moove_gdpr_consent_expiration',	'60',	1,	NULL),
(18,	'moove_gdpr_modal_powered_by_label',	'Powered by',	1,	NULL),
(19,	'moove_gdpr_brand_colour',	'#e22c2c',	1,	NULL),
(20,	'moove_gdpr_brand_secondary_colour',	'#000000',	1,	NULL),
(21,	'moove_gdpr_company_logo',	'/wp-content/plugins/gdpr-cookie-compliance/dist/images/gdpr-logo.png',	1,	NULL),
(22,	'moove_gdpr_logo_position',	'left',	1,	NULL),
(23,	'moove_gdpr_button_style',	'rounded',	1,	NULL),
(24,	'moove_gdpr_plugin_font_type',	'3',	1,	NULL),
(25,	'moove_gdpr_plugin_font_family',	'\'Montserrat\', sans-serif',	1,	NULL),
(26,	'moove_gdpr_cdn_url',	'',	1,	NULL);";

$wpdb->query($sql_gdpr_insert);
}

add_filter( 'facetwp_query_args', function( $query_args, $class ) {
    // if ( 'luxury_vinyl_tile' == $class->ajax_params['template'] ) {
        $query_args['posts_per_page']='12';
        $query_args['meta_key']='collection';
        if(($query_args['post_type'] !="fl-builder-template" && $query_args['post_type'] !="post") &&  @wp_count_posts($query_args['post_type'])->publish > 1 ){
            add_filter('posts_groupby', 'query_group_by_filter');
        }
    // }
    return $query_args;
}, 10, 2 );

/*Thank you content*/
function display_thankyoumsg(){
    
    if($_GET['promocode'] =='shwbtfll2020')
    {
    $post_contet_data = '<style>.thank-you-hide {display:none !important;}.thank-you-show {display:block !important;}</style>';  
    $post_contet_data .= '[fl_builder_insert_layout slug="thankyou-image"]';   
    } else {
    $post_contet_data = '<style>.thank-you-hide {display:block !important;}.thank-you-show {display:none !important;}</style>'; 
    //$post_contet_data = '[fl_builder_insert_layout slug="thankyou-content"]';   
    }
    //$post_contet_data ='';
    return $post_contet_data;
}
add_shortcode( 'display_thankyoumsg', 'display_thankyoumsg' );


if (! wp_next_scheduled ( 'seo_titles_update_job')) {
     
    wp_schedule_event(  strtotime('09:00:00'), 'daily', 'seo_titles_update_job');
}
add_action( 'seo_titles_update_job', 'do_seo_titles_update', 10, 2 );

function do_seo_titles_update(){

    $apiObj = new APICaller;
    $inputs = array('grant_type'=>'client_credentials','client_id'=>get_option('CLIENT_CODE'),'client_secret'=>get_option('CLIENTSECRET'));
    $result = $apiObj->call(AUTHURL,"POST",$inputs,array(),AUTH_BASE_URL);

    if(isset($result['error'])){
        $msg =$result['error'];                
        $_SESSION['error'] = $msg;
        $_SESSION["error_desc"] =$result['error_description'];
        
    }
    else if(isset($result['access_token'])){

        $inputs = array();
        $headers = array('authorization'=>"bearer ".$result['access_token']);
        $seo_titles = $apiObj->call(BASEURL.get_option('SITE_CODE')."/".SEOURL,"GET",$inputs,$headers);

        write_log($seo_titles['result']);
       

        foreach($seo_titles['result'] as $seo){

            write_log($seo['title']);

            $search_page =    url_to_postid($seo['title']);

            write_log($search_page);

            if($seo['title'] == '/' && $seo['page']== 'Home'){

                $pageID = get_option('page_on_front');

                update_post_meta( $pageID, '_yoast_wpseo_title', $seo['seoTitle'] );
            }

            
            update_post_meta( $search_page, '_yoast_wpseo_title', $seo['seoTitle'] );
            
        }
    }
}

function get_companyname() {
    $website = json_decode(get_option('website_json'));

    for($j=0;$j<count($website->sites);$j++){        
        return  $website->sites[$j]->name;
    }    
}

// define the action for register yoast_variable replacments
function register_custom_yoast_variables() {
    wpseo_register_var_replacement( "%%[Retailer 'companyname']%%", 'get_companyname', 'advanced', 'some help text' );
}

// Add action
add_action('wpseo_register_extra_replacements', 'register_custom_yoast_variables');

function compare_csv_onsite_products($delete_posttype , $carpet_manufacturer = null){

    write_log('compare_csv_onsite_products Started');    

    global $wpdb;
    $product_table = $wpdb->prefix."posts";
    $table_redirect = $wpdb->prefix.'redirection_items';  
    $table_group = $wpdb->prefix.'redirection_groups';
    $datum = $wpdb->get_results("SELECT * FROM $table_group WHERE name = 'Products'");
    $redirect_group =  $datum[0]->id;  

    $table_posts = $wpdb->prefix.'posts';
	$table_meta = $wpdb->prefix.'postmeta';	   
    
    if($delete_posttype == "carpeting"){

        write_log('yes its carpeting');

        write_log('tested-------'.$delete_posttype.'-------'.$carpet_manufacturer);    

        $sql_sku = "SELECT wp_posts.ID FROM wp_posts  LEFT JOIN wp_postmeta ON ( wp_posts.ID = wp_postmeta.post_id )  LEFT JOIN wp_postmeta AS mt1 ON (wp_posts.ID = mt1.post_id AND mt1.meta_key = 'endpoint' ) WHERE 1=1  AND ( 
            ( wp_postmeta.meta_key = 'manufacturer' AND wp_postmeta.meta_value = '$carpet_manufacturer' ) AND mt1.post_id IS NULL) AND wp_posts.post_type = 'carpeting' AND ((wp_posts.post_status = 'publish')) GROUP BY wp_posts.ID ORDER BY wp_posts.post_date DESC ";


        $old_products = $wpdb->get_results($sql_sku);   

    }else{

        $sql_sku = "SELECT p.ID
                    FROM  $table_posts p LEFT JOIN $table_meta pm
                    ON p.ID = pm.post_id AND pm.meta_key = 'endpoint'
                    WHERE p.post_type = '$delete_posttype'
                    AND ifnull(pm.meta_value, '') = ''" ;           
            
            $old_products = $wpdb->get_results($sql_sku);   
        
    }
    

    $brandmapping = array(
        "/flooring/carpet/products/"=>"carpeting",
        "/flooring/hardwood/products/"=>"hardwood_catalog",
        "/flooring/laminate/products/"=>"laminate_catalog",
        "/flooring/"=>"luxury_vinyl_tile",
        "/flooring/tile/products/"=>"tile_catalog",
        "/flooring/waterproof/"=>"solid_wpc_waterproof"
    );


        foreach ($old_products as $delete_post ){

                        write_log( 'Current Loop -----'.$i.'--ID--'.$delete_post->ID);
                    

                        $sku = get_post_meta($delete_post->ID,'sku', true);

                        $post_id = $delete_post->ID;

                        write_log( $post_id.'-------Post Deleted if found any');                       
          

                        $source_url = wp_make_link_relative(get_the_guid($post_id));
                        $match_url = rtrim($source_url, '/');

                        write_log($source_url);

                        $post_type =  get_post_type($post_id );

                        $destination_url = array_search($post_type,$brandmapping);

                        write_log($destination_url);

                        $data = array(
                        "url" => $source_url,
                        "match_url" => $match_url,
                        "match_data" => "",
                        "action_code" => "301",
                        "action_type" => "url",
                        "action_data" => $destination_url,
                        "match_type" => "url",
                        "title" => $sku,
                        "regex" => "true",
                        "group_id" => $redirect_group,
                        "position" => "1",
                        "last_access" => current_time( 'mysql' ),
                        "status" => "enabled"
                    );

                    

                        $format = array('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s','%s');

                        $wpdb->insert($table_redirect,$data,$format);

                        wp_delete_post($post_id);

                        write_log('Post Deleted form Database');

                  
            }
                                 
                    
                    
                    write_log('compare_csv_onsite_products ended' );

}


wp_clear_scheduled_hook( 'finalsync_lvt_thursday_event' );
// if (! wp_next_scheduled ( 'finalsync_lvt_thursday_event')) {      

//     $interval =  getIntervalTime('thursday');    
//     wp_schedule_event( time() + 3800 , 'each_thursday', 'finalsync_lvt_thursday_event');
// }

// add_action( 'finalsync_lvt_thursday_event', 'do_this_thursday_lvt', 10, 2 );

 // Clear light speed serverr cache for every 6 hours

 function lightspeed_custom_cron_schedule( $schedules ) {
    $schedules['every_six_hours'] = array(
        'interval' => 21600, // Every 6 hours
        'display'  => __( 'Every 6 hours' ),
    );
    return $schedules;
}
add_filter( 'cron_schedules', 'lightspeed_custom_cron_schedule' );
 
if (! wp_next_scheduled ( 'lightspeed_clear_cache')) {

        $interval =  getIntervalTime('thursday'); 
        wp_schedule_event(time() , 'every_six_hours', 'lightspeed_clear_cache');
}

add_action( 'lightspeed_clear_cache', 'lightspeed_clear_cache_function' );
 
function lightspeed_clear_cache_function(){
    
    if ( defined(LSCWP_V)) {

        write_log('Light Speed server cache clearing started');

        do_action( 'litespeed_purge_all' );

        write_log('Light Speed cache cleared');

    }else{

        write_log('Light Speed cache Plugin is not installed');
    }

}